angular

    .module('starter.factories', [])

    .constant('API_TOKEN', 'LoskiRot160816')

    .constant('API_URL', 'http://portal.infinvesting.com')
    // .constant('API_URL', 'http://dev.sye.co.il')

    .factory('API', function ($localStorage, $timeout, $q, $resource, API_TOKEN, API_URL, $http, $window, $httpParamSerializerJQLike) {

        return {

            // Registration

            register: function (data) {

                return $resource(API_URL + '/RealReg_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "FirstName": data.FirstName,
                        "LastName": data.LastName,
                        "Email": data.Email,
                        "Password": data.Password,
                        "1stPhone": data.FirstPhone,
                        "Birthday": data.Birthday,
                        "Country": data.Country,
                        // "UseCurrencyByDefault": data.Currency,
                        "Confirm_TermsOfUse": data.ConfirmTermsOfUse,
                        "Confirm_TradeContract": data.ConfirmTradeContract,
                        "SpareStrFld01": data.Robot

                    }).$promise.then(
                        function (data) {

                            data = data.toJSON();
                            console.log(data);
                            // alert(JSON.stringify(data));

                            if (data.Registration != 'OK') {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {
                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {
                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            // Login

            login: function (data) {

                return $resource(API_URL + '/Login_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "Leading_ID": data.Email,
                        "Password": data.Password

                    }).$promise.then(
                        function (data) {

                            data = data.toJSON();

                            if (data.Login != 'OK') {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }
                                console.log("here goes error");
                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            // Logout

            logout: function (data) {

                return $resource(API_URL + '/Logout_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID

                    }).$promise.then(
                        function (data) {

                            data = data.toJSON();

                            if (data.Logout != 'OK') {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            // Forgot password

            sendForgotPassword: function (data) {

                return $resource(API_URL + '/Forgot_Password_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "Email": data.Email

                    }).$promise.then(
                        function (data) {

                            data = data.toJSON();

                            if (data.Forgot_Password != 'OK') {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },


            robotaccounts: function (data) {

                return $resource(API_URL + '/Standard_TP_Portal_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID

                    }).$promise.then(

                        function (data) {

                            data = data.toJSON();

                            if (!data.Standard_TP_Portal) {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            makerobot: function (data) {

                return $resource(API_URL + '/Add_Real_TP_Account_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID,
                        "FirstName": data.FirstName,
                        "LastName": data.LastName,
                        "Email": data.Email,
                        "Password": data.Password,
                        "1stPhone": data.FirstPhone,
                        "Birthday": data.Birthday,
                        "Country": data.Country,
                        // "UseCurrencyByDefault": data.Currency,
                        "Confirm_TermsOfUse": "yes",
                        "Confirm_TradeContract": "yes",
                        "AccountType": "Real",
                        // "SpareStrFld01": data.SpareStrFld01
                        "TPGroup": data.SpareStrFld01

                    }).$promise.then(

                        function (data) {

                            console.log(data);

                            data = data.toJSON();

                            if (data.Add_Another_TP_Account != 'OK') {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            withdraw: function (data) {

                return $resource(API_URL + '/TP_Withdraw_Funds_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID,
                        "Amount": data.Amount,
                        "PaymentMethod": data.PaymentMethod,
                        "ClientNumber": data.ClientNumber

                    }).$promise.then(

                        function (data) {

                            console.log(data);

                            data = data.toJSON();

                            if (data.Withdrawal_json != 'OK') {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            // Personal information

            details: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID

                };

                return $http.post(API_URL + '/Personal_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.Title) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // change phone in information

            // changedata: function (data) {
            //
            //     var payload = {
            //
            //         "ApiToken": API_TOKEN,
            //         "SessionID": data.SessionID,
            //         "Country" : data.country
            //
            //     };
            //
            //     return $http.post(API_URL + '/Personal_json',  $httpParamSerializerJQLike(payload), {
            //
            //         headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            //         paramSerializer: '$httpParamSerializer'
            //
            //     }).then(function (data) {        // on success
            //
            //         if (!data.data.PersonalData && data.data.PersonalData != "Modified") {
            //
            //             if (data.data.ResponseCode) {
            //
            //                 return $q.reject({
            //
            //                     error: data.data
            //
            //                 });
            //
            //             } else if (data.data.PersonalData == "NoChange") {
            //
            //                 return $q.reject({
            //
            //                     error: {
            //
            //                         ResponseCode: "997",
            //                         ResponseMsgEng: "The settings were not modified!"
            //
            //                     }
            //
            //                 });
            //
            //             }
            //
            //             return $q.reject({
            //
            //                 error: {
            //
            //                     ResponseCode: "998",
            //                     ResponseMsgEng: "No connection to database!"
            //
            //                 }
            //
            //             });
            //
            //         }
            //
            //         return data;
            //
            //     }, function (data) {             // on error
            //
            //         return $q.reject({
            //
            //             error: {
            //
            //                 ResponseCode: "999",
            //                 ResponseMsgEng: "No network connection!"
            //
            //             }
            //
            //         });
            //
            //     });
            //
            // },

            // change password

            changepassword: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "CurrentPassword": data.CurrentPassword,
                    "NewPassword": data.NewPassword,
                    "ConfirmPassword": data.ConfirmPassword,
                    "TP_Pass_cb" : "Yes"

                };

                return $http.post(API_URL + '/Change_Site_Password_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.Change_Site_Password) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // send Public Service Ticket = contact us

            contact: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "ComplainerName": data.ComplainerName,
                    "SubjectComplain": data.SubjectComplain,
                    "Phone": data.Phone,
                    // "Leading_ID": data.Email
                    "Email": data.Email

                };

                return $http.post(API_URL + '/Public_Service_Ticket_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    console.log(data);

                    if (!data.data.Public_Service_Ticket) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // get documents

            getDocumentsTree: function (data) {

                return $resource(API_URL + '/GetDocsTree_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID

                    }).$promise.then(

                        function (data) {

                            data = data.toJSON();

                            if (!data.GetDocsTree) {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            var docs = {};

                            docs = data.GetDocsTree;

                            // for (var i = 0; i < docs.length; i++) {
                            //
                            //     for (var j = 0; j < docs[i].FilesList.length; j++) {
                            //
                            //         var filename = docs[i].FilesList[j].FileName.replace(' ', '%20');
                            //
                            //         var fileaddress = "http://portal.infinvesting.com/Download_Doc?SessionID=" + $localStorage.session + "&fn=" + filename;
                            //
                            //         docs[i].FilesList[j]['src'] = fileaddress;
                            //
                            //     }
                            // }
                            //
                            // console.log(docs);

                            return docs;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            // upload photo

            uploadDoc: function (data, payload, sendName) {

                var deferred = $q.defer();

                var options = new FileUploadOptions();

                options.fileName = sendName;

                if (sendName.substr(sendName.lastIndexOf(".") + 1) === "pdf") {

                    options.mimeType = "application/pdf";

                } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "jpg" || sendName.substr(sendName.lastIndexOf(".") + 1) === "jpeg") {

                    options.mimeType = "image/jpeg";

                } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "png") {

                    options.mimeType = "image/png";

                } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "tiff" || sendName.substr(sendName.lastIndexOf(".") + 1) === "tif") {

                    options.mimeType = "image/tiff";

                } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "doc") {

                    options.mimeType = "application/msword";

                } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "docx") {

                    options.mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                }

                // options.mimeType = "image/jpeg";

                options.fileKey = "uploaded_file";

                options.params = payload;

                var ft = new FileTransfer();

                ft.upload(data, encodeURI(API_URL + '/Upload_Doc_json'),

                    function (data) {

                        // alert(JSON.stringify(data));

                        var answer = null;

                        try {

                            answer = JSON.parse(data.response);

                        } catch(e){

                            // alert(e);

                            deferred.reject({

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            });

                        }

                        if (answer.ResponseCode) {

                            deferred.reject(answer);

                        } else {

                            deferred.resolve(answer);

                        }

                    },

                    function (response) {

                        deferred.reject({

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        });

                    }, options);

                return deferred.promise;

            },

            // delete photo

            deleteDoc: function (data) {

                return $resource(API_URL + '/Delete_Doc_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID,
                        "fn": data.fn

                    }).$promise.then(
                        function (data) {

                            data = data.toJSON();

                            if (data.Delete_Doc !== "OK") {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            downloadDocument: function (file, data) {

                var send_data = {
                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "fn": data.fn
                };

                $http.post(API_URL + '/Download_Doc_json', $httpParamSerializerJQLike(send_data), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    responseType: 'blob',
                    // timeout: 5000,
                    paramSerializer: '$httpParamSerializer'

                }).then(function (response) {

                    if (response.headers('Content-Type') == 'image/jpeg') {

                        var reader = new $window.FileReader();
                        reader.readAsDataURL(response.data);
                        reader.onloadend = function () {
                            base64data = reader.result;
                            file.src = base64data;

                        }
                    } else {
                        console.log('Can not recognize it');
                    }


                }, function (response) {

                    console.log('Timeout error');

                });


                // $http.get(API_URL + '/Download_Doc_json', {
                //
                //     params: {
                //         "ApiToken": API_TOKEN,
                //         "SessionID": data.SessionID,
                //         "fn": data.fn
                //     },
                //     responseType: 'blob',
                //     timeout: 5000
                //
                // }).then(function (response) {
                //
                //     if (response.headers('Content-Type') == 'image/jpeg') {
                //
                //         var reader = new $window.FileReader();
                //         reader.readAsDataURL(response.data);
                //         reader.onloadend = function () {
                //             base64data = reader.result;
                //             file.src = base64data;
                //
                //         }
                //     } else {
                //         console.log('Can not recognize it');
                //     }
                //
                // }, function (response) {
                //
                //     console.log('Timeout error');
                //
                // });

            },

            // get all tickets

            support: function (data) {

                return $resource(API_URL + '/Service_Ticket_json')

                    .get({

                        "ApiToken": API_TOKEN,
                        "SessionID": data.SessionID

                    }).$promise.then(

                        function (data) {

                            data = data.toJSON();

                            if (!data.Title) {

                                if (data.ResponseCode) {

                                    return $q.reject({

                                        error: data

                                    });

                                }

                                return $q.reject({

                                    error: {

                                        ResponseCode: "998",
                                        ResponseMsgEng: "No connection to database!"

                                    }

                                });

                            }

                            return data;

                        },

                        function (err) {

                            return $q.reject({

                                error: {

                                    ResponseCode: "999",
                                    ResponseMsgEng: "No network connection!"

                                }

                            });

                        });

            },

            // send new ticket

            newticket: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "Title": data.Title,
                    "SubjectComplain": data.SubjectComplain

                };

                return $http.post(API_URL + '/Service_Ticket_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.Service_Ticket) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // get all inbox messages

            inbox: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "Action" : "Get_All_Messages"

                };

                return $http.post(API_URL + '/Inbox_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.Title) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // delete inbox message

            deletemessage: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "Action" : "Delete_Message",
                    "Key" : data.Key

                };

                return $http.post(API_URL + '/Inbox_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.InboxMessage) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // mark inbox message as read

            readmessage: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "Action" : "Mark_Read_Message",
                    "Key" : data.Key

                };

                return $http.post(API_URL + '/Inbox_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.InboxMessage) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // get all data about trade accounts for this user

            accounts: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID

                };

                return $http.post(API_URL + '/TP_Deposit_By_Hyper_json',  $httpParamSerializerJQLike(payload), {
                // return $http.post(API_URL + '/Get_Accounts_List_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    if (!data.data.Title) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            // creditcarddeposit: function (data) {
            //
            //     var payload = {
            //
            //         "ApiToken": API_TOKEN,
            //         "SessionID": data.SessionID,
            //         "ClientNumber" : data.ClientNumber,
            //         "TotalAmount" : data.TotalAmount,
            //         "Currency" : data.Currency,
            //         "CardNumber" : data.CardNumber,
            //         "Validity" : data.Validity,
            //         "CVV" : data.CVV,
            //         "CardHolder" : data.CardHolder
            //
            //     };
            //
            //     return $http.post(API_URL + '/TP_Deposit_By_Hyper_json',  $httpParamSerializerJQLike(payload), {
            //
            //         headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            //         paramSerializer: '$httpParamSerializer'
            //
            //     }).then(function (data) {        // on success
            //
            //         if (!data.data.TP_Deposit_By_Hyper_json) {
            //
            //             if (data.data.ResponseCode) {
            //
            //                 return $q.reject({
            //
            //                     error: data.data
            //
            //                 });
            //
            //             }
            //
            //             return $q.reject({
            //
            //                 error: {
            //
            //                     ResponseCode: "998",
            //                     ResponseMsgEng: "No connection to database!"
            //
            //                 }
            //
            //             });
            //
            //         }
            //
            //         return data;
            //
            //     }, function (data) {             // on error
            //
            //         return $q.reject({
            //
            //             error: {
            //
            //                 ResponseCode: "999",
            //                 ResponseMsgEng: "No network connection!"
            //
            //             }
            //
            //         });
            //
            //     });
            //
            // },

            // wire transfer deposit

            wiretransferdeposit: function (data, payload, sendName) {

                var deferred = $q.defer();

                var options = new FileUploadOptions();

                options.fileName = sendName;

                // if (sendName.substr(sendName.lastIndexOf(".") + 1) === "pdf") {
                //
                //     options.mimeType = "application/pdf";
                //
                // } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "jpg" || sendName.substr(sendName.lastIndexOf(".") + 1) === "jpeg") {
                //
                //     options.mimeType = "image/jpeg";
                //
                // } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "png") {
                //
                //     options.mimeType = "image/png";
                //
                // } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "tiff" || sendName.substr(sendName.lastIndexOf(".") + 1) === "tif") {
                //
                //     options.mimeType = "image/tiff";
                //
                // } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "doc") {
                //
                //     options.mimeType = "application/msword";
                //
                // } else if (sendName.substr(sendName.lastIndexOf(".") + 1) === "docx") {
                //
                //     options.mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                //
                // }

                options.mimeType = "image/jpeg";

                options.fileKey = "FileUpload";

                options.params = payload;

                var ft = new FileTransfer();

                ft.upload(data, encodeURI(API_URL + '/TP_Bank_Transfer_Upload_json'),

                    function (data) {

                        // alert(data.response);

                        var answer = null;

                        try {

                            answer = JSON.parse(data.response);

                        } catch(e){

                            // alert(e);

                            deferred.reject({

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            });

                        }


                        if (answer && answer.ResponseCode) {

                            if(answer.ResponseCode == '022') {

                                deferred.resolve(answer);

                            } else {

                                deferred.reject(answer);

                            }

                        }

                        deferred.reject({

                            ResponseCode: "998",
                            ResponseMsgEng: "No connection to database!"

                        });

                    },

                    function (response) {

                        deferred.reject({

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        });

                    }, options);

                return deferred.promise;

            },

            fakedeposit: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    "ClientNumber": data.ClientNumber,
                    "TotalAmount": data.TotalAmount,
                    "Currency": data.Currency,
                    "Reference" : data.Reference,
                    "CC_Brand": data.CC_Brand

                };

                return $http.post(API_URL + '/TP_Deposit_CC_by_3rd_Party_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    console.log(data.data);

                    if (!data.data.TP_Deposit_CC_by_3rd_Party_json) {

                        if (data.data.ResponseCode) {

                            return $q.reject({

                                error: data.data

                            });

                        }

                        return $q.reject({

                            error: {

                                ResponseCode: "998",
                                ResponseMsgEng: "No connection to database!"

                            }

                        });

                    }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },

            journal: function (data) {

                var payload = {

                    "ApiToken": API_TOKEN,
                    "SessionID": data.SessionID,
                    // "ForAccount" : "500069"

                };

                return $http.post(API_URL + '/Journal_Report_json',  $httpParamSerializerJQLike(payload), {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                }).then(function (data) {        // on success

                    console.log(data.data);

                    // if (!data.data.TP_Deposit_By_Hyper_json) {
                    //
                    //     if (data.data.ResponseCode) {
                    //
                    //         return $q.reject({
                    //
                    //             error: data.data
                    //
                    //         });
                    //
                    //     }
                    //
                    //     return $q.reject({
                    //
                    //         error: {
                    //
                    //             ResponseCode: "998",
                    //             ResponseMsgEng: "No connection to database!"
                    //
                    //         }
                    //
                    //     });
                    //
                    // }

                    return data;

                }, function (data) {             // on error

                    return $q.reject({

                        error: {

                            ResponseCode: "999",
                            ResponseMsgEng: "No network connection!"

                        }

                    });

                });

            },


        };



    })

;

