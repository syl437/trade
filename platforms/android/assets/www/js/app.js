// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.factories', 'ngResource', 'ngCordova', 'ngStorage',
    'credit-cards', 'puigcerber.countryPicker', 'nvd3', 'internationalPhoneNumber', 'pascalprecht.translate'])

    .run(function ($ionicHistory, API, $timeout, $ionicPlatform, $rootScope, $localStorage, $ionicPopup, $state, $http, $translate) {

        $ionicPlatform.ready(function () {

            // Default code

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            // Notifications

            var notificationOpenedCallback = function (jsonData) {

                if (jsonData.additionalData.type == "newNotification") {

                    // alert(JSON.stringify(jsonData));
                    $rootScope.newNotification = jsonData;

                    if (jsonData.additionalData.title == 'atech ATF') {

                        $rootScope.$broadcast('newNotificationToAll');
                        $state.go('app.myaccount');

                    } else {

                        // alert('123');
                        $rootScope.$broadcast('newNotification');
                        $state.go('app.investment');

                    }

                }

                // console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
            };

            window.plugins.OneSignal.init("55000a23-cf3b-468b-bcdc-4ea79fd7eb67",
                {googleProjectNumber: "595323574268"},
                notificationOpenedCallback);

            window.plugins.OneSignal.getIds(function (ids) {

                $rootScope.pushId = ids.userId;

            });

            // Show an alert box if a notification comes in when the user is in your app.
            window.plugins.OneSignal.enableInAppAlertNotification(true);

            // Back button

            $ionicPlatform.registerBackButtonAction(function (event)
            {
                if($state.current.name == 'app.login') {

                    // do nothing

                }  else {

                    $ionicHistory.goBack();

                }

            },100);

        });

        // TODO: delete it if not necessary

        document.addEventListener("deviceready", onDeviceReady, false);
        function onDeviceReady() {
            console.log(navigator.globalization);
        }

        // global variables

        $rootScope.host = 'http://portal.infinvesting.com';
        $rootScope.tapperHost = 'http://www.tapper.co.il/trade/php';
        $rootScope.currentState = "";
        $rootScope.unreadInbox = 0;
        // $rootScope.currencies = ['USD', 'EUR', 'GBP', 'AUD', 'CAD'];
        $rootScope.newNotification = '';
        $rootScope.phoneVerified = $localStorage.phoneVerified;
        $rootScope.clientPhone = $localStorage.clientPhone;
        $rootScope.step = $localStorage.step;
        $rootScope.firstRobot = "adam";
        $rootScope.firstRobotTPname = "";
        $rootScope.robot = "adam";
        $rootScope.robotTPname = "";
        // $rootScope.adamTPname = "demoAT-1-EC11d";
        // $rootScope.emilyTPname = "demoAT-USD";
        // $rootScope.brunoTPname = "demoAT-1-SP12";
        $rootScope.adamTPname = "demoAT-1-ATF1";
        $rootScope.emilyTPname = "demoAT-1-ATF3";
        $rootScope.brunoTPname = "demoAT-1-ATF2";
        // $rootScope.userLocale = "";
        $rootScope.information = {};
        $rootScope.creditCard = {"amount" : "", "robot" : ""};
        $rootScope.robotAccountsList = [];
        $rootScope.robotAccountsState = [];
        $rootScope.activeRobots = {};
        $rootScope.accountsList = [];
        $rootScope.selectedLanguage = {'lang' : ""};
        $rootScope.totalBalance = 0;
        $rootScope.totalEquity = 0;

        if (typeof ($localStorage.language) == 'undefined' || $localStorage.language == "" || !$localStorage.language){

            $rootScope.selectedLanguage.lang = "en";

        } else {

            $rootScope.selectedLanguage.lang = $localStorage.language;

        }

        // languages

        $rootScope.$watch("selectedLanguage.lang", function(){

            $localStorage.language = $rootScope.selectedLanguage.lang;
            $translate.use($localStorage.language);

        });

        // displaying standard error messages

        $rootScope.errorMessage = function(data){

            if (data.error.ResponseCode === "006"){

                $ionicPopup.alert({
                    title: $translate.instant("ERR1"),  // session is over
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                }).then(function(){

                    $state.go('app.login')

                });

            } else if (data.error) {

                $ionicPopup.alert({
                    title: data.error.ResponseMsgEng,
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                $ionicPopup.alert({
                    title: $translate.instant("ERR2"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            }

        };

        // for getting all messages

        $rootScope.tickets = [];

        $rootScope.messages = [];

        $rootScope.inbox = [];

        $rootScope.getInbox = function(){

            $rootScope.tickets = [];
            $rootScope.messages = [];
            $rootScope.inbox = [];
            $rootScope.unreadInbox = 0;

            API.inbox({

                "SessionID": $localStorage.session

            }).then(function (data) {

                $rootScope.messages = data.data.Data;

                for (var i = 0; i < $rootScope.messages.length; i++){

                    $rootScope.messages[i].CreatedOn = Date.parse($rootScope.messages[i].CreatedOn);
                    $rootScope.messages[i].type = "message";

                    $rootScope.inbox.push($rootScope.messages[i]);

                }

                console.log("Messages", $rootScope.messages);

                // get all tickets

                API.support({

                    "SessionID": $localStorage.session

                }).then(function (data) {

                    $rootScope.tickets = data.Data;

                    for (var i = 0; i < $rootScope.tickets.length; i++){

                        $rootScope.tickets[i].CreatedOn = Date.parse($rootScope.tickets[i].CreatedOn);
                        $rootScope.tickets[i].type = "ticket";

                        $rootScope.inbox.push($rootScope.tickets[i]);

                    }

                    // assembling inbox

                    for (var w = 0; w < $rootScope.inbox.length; w++) {

                        $rootScope.inbox[w].index = w;

                        if($rootScope.inbox[w].UserReadMessageOn == ""){

                            $rootScope.unreadInbox += 1;

                        }

                    }

                    console.log("Tickets", $rootScope.tickets);

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }, function (response) {

                $rootScope.errorMessage(response);

            });

            console.log("Inbox", $rootScope.inbox);

        };

        // get personal information

        $rootScope.getPersonalInformation = function(){

            API.details({

                "SessionID": $localStorage.session

            }).then(function (data) {

                $rootScope.information = data.data.Data[0];
                console.log("Information", $rootScope.information);

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

        // synchronizing Complete Deposit button at app.investment and robots at Deposit

        $rootScope.$on("completeDeposit", function(event, data){

            for (var f = 0; f < $rootScope.accountsList.length; f++){

                if ($rootScope.accountsList[f].ClientNumber == data.clientIndex){

                    $rootScope.creditCard.robot = $rootScope.accountsList[f];

                }

            }

        });


        // get information about opened robots

        $rootScope.getRobotAccounts = function(){

            $rootScope.totalBalance = 0;
            $rootScope.totalEquity = 0;

            API.robotaccounts({

                "SessionID": $localStorage.session

            }).then(function (data) {

                for (var k = 0; k < data.Standard_TP_Portal.length; k++){

                    if (data.Standard_TP_Portal[k].Title == "Accounts_List"){

                        $rootScope.robotAccountsList = data.Standard_TP_Portal[k].Data;

                    }

                    if (data.Standard_TP_Portal[k].Title == "Accounts_State"){

                        $rootScope.robotAccountsState = data.Standard_TP_Portal[k].Data;

                    }

                }

                for (var l = 0; l < $rootScope.robotAccountsState.length; l++){

                    if ($rootScope.robotAccountsState[l].SpareStrFld01 == $rootScope.adamTPname){

                        $rootScope.activeRobots["adam"] = $rootScope.robotAccountsState[l];

                    }

                    if ($rootScope.robotAccountsState[l].SpareStrFld01 == $rootScope.emilyTPname){

                        $rootScope.activeRobots["emily"] = $rootScope.robotAccountsState[l];

                    }

                    if ($rootScope.robotAccountsState[l].SpareStrFld01 == $rootScope.brunoTPname){

                        $rootScope.activeRobots["bruno"] = $rootScope.robotAccountsState[l];

                    }

                    $rootScope.totalBalance += Number(Number($rootScope.robotAccountsState[l].SpareExtFld02).toFixed(2));
                    $rootScope.totalEquity += Number(Number($rootScope.robotAccountsState[l].SpareExtFld03).toFixed(2));

                }

                console.log("robotAccountsList", $rootScope.robotAccountsList);
                console.log("robotAccountsState", $rootScope.robotAccountsState);
                console.log("activeRobots", $rootScope.activeRobots);

                // checking if some robot has deposits

                var send_data = {

                    "id": $localStorage.tapperId

                };

                $http.post($rootScope.tapperHost + "/checkDeposits.php", send_data, {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })

                    .then(function (data) {   // on success

                            console.log("RobotsWithDeposit", data);

                            for (var t = 0; t < data.data.length; t++){

                                $rootScope.activeRobots[data.data[t]]['isDeposited'] = true;

                            }

                            // then get all accounts for deposit

                            $rootScope.getAccountsList();

                        },

                        function (err) {      // on error

                            // alert(err);

                        })

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

        // get accounts list for deposit

        $rootScope.getAccountsList = function(){

            API.accounts({

                "SessionID": $localStorage.session

            }).then(function (data) {

                $rootScope.accountsList = data.data.Data;
                $rootScope.accountsList.reverse();

                // adding data for easier managing

                for (var i = 0; i < $rootScope.accountsList.length; i++){

                    for (var j = 0; j < $rootScope.robotAccountsState.length; j++){

                        if ($rootScope.accountsList[i].ClientNumber == $rootScope.robotAccountsState[j].ClientNumber){

                            $rootScope.accountsList[i].SpareStrFld01 = $rootScope.robotAccountsState[j].SpareStrFld01;
                            $rootScope.accountsList[i]["AccountNumber"] = $rootScope.robotAccountsState[j].SpareExtFld01;
                            // $rootScope.accountsList[i]["isDeposited"] = (typeof $rootScope.robotAccountsState[j]["isDeposited"] != 'undefined') ? $rootScope.robotAccountsState[j].isDeposited : false;

                            if ($rootScope.accountsList[i].SpareStrFld01 == $rootScope.adamTPname){

                                $rootScope.accountsList[i]["RobotName"] = "Adam ATF";
                                $rootScope.accountsList[i]["SmallRobotName"] = "adam";

                            }

                            if ($rootScope.accountsList[i].SpareStrFld01 == $rootScope.emilyTPname){

                                $rootScope.accountsList[i]["RobotName"] = "Emily ATF";
                                $rootScope.accountsList[i]["SmallRobotName"] = "emily";

                            }

                            if ($rootScope.accountsList[i].SpareStrFld01 == $rootScope.brunoTPname){

                                $rootScope.accountsList[i]["RobotName"] = "Bruno ATF";
                                $rootScope.accountsList[i]["SmallRobotName"] = "bruno";

                            }

                        }

                    }

                }

                console.log("Accounts", $rootScope.accountsList);
                console.log($rootScope.totalBalance, $rootScope.totalEquity);

                $rootScope.creditCard.robot = $rootScope.accountsList[0];

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider, ipnConfig) {

        // var userLocale = "";
        //
        // // defining locale
        //
        // document.addEventListener("deviceready", onDeviceReady, false);
        //
        // function onDeviceReady() {
        //     window.plugins.sim.getSimInfo(successCallback, errorCallback);
        // }
        //
        // function successCallback(result) {
        //     alert(userLocale);
        //      userLocale = result.countryCode;
        // }
        //
        // function errorCallback(error) {
        //     console.log(error);
        // }

        // ipnConfig.defaultCountry = (userLocale != '') ? userLocale : "";

        // Translations

        $translateProvider.useStaticFilesLoader({
            prefix: 'languages/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');
        $translateProvider.fallbackLanguage('en');

        // Router

        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.enter', {
                url: '/enter',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/enter.html',
                        controller: 'EnterCtrl'
                    }
                }
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('app.register', {
                url: '/register',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/register.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('app.phone', {
                url: '/phone',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/phone_verification.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('app.chooseATF', {
                url: '/chooseATF',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/choose_atf.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('app.policy', {
                url: '/policy',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/policy.html',
                        controller: 'TermsCtrl'
                    }
                }
            })

            .state('app.terms', {
                url: '/terms',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/terms.html',
                        controller: 'TermsCtrl'
                    }
                }
            })

            .state('app.risk', {
                url: '/risk',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/risk.html',
                        controller: 'TermsCtrl'
                    }
                }
            })

            .state('app.myaccount', {
                url: '/myaccount',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/my_account.html',
                        controller: 'MyAccountCtrl'
                    }
                }
            })

            .state('app.investment', {
                url: '/investment',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/investment.html',
                        controller: 'InvestmentCtrl'
                    }
                }
            })

            .state('app.reports', {
                url: '/reports',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/reports.html',
                        controller: 'ReportsCtrl'
                    }
                }
            })

            .state('app.deposit', {
                url: '/deposit',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/deposit.html',
                        controller: 'DepositCtrl'
                    }
                }
            })

            .state('app.history', {
                url: '/history',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/history.html',
                        controller: 'HistoryCtrl'
                    }
                }
            })

            .state('app.withdrawal', {
                url: '/withdrawal',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/withdrawal.html',
                        controller: 'WithdrawalCtrl'
                    }
                }
            })

            .state('app.documents', {
                url: '/documents',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/documents.html',
                        controller: 'DocumentsCtrl'
                    }
                }
            })

            .state('app.information', {
                url: '/information',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/information.html',
                        controller: 'InformationCtrl'
                    }
                }
            })

            .state('app.support', {
                url: '/support',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/support.html',
                        controller: 'SupportCtrl'
                    }
                }
            })

            .state('app.inbox', {
                url: '/inbox',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/inbox.html',
                        controller: 'InboxCtrl'
                    }
                }
            })

            .state('app.notifications', {
                url: '/notifications',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/notifications.html',
                        controller: 'NotificationsCtrl'
                    }
                }
            })

            .state('app.model', {
                url: '/model',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/model.html',
                        controller: 'TermsCtrl'
                    }
                }
            })

            .state('app.faq', {
                url: '/faq',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/faq.html',
                        controller: 'FAQCtrl'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/enter');

        $ionicConfigProvider.backButton.previousTitleText(false).text('');
    });
