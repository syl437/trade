angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($translate, $scope, API, $ionicPopup, $state, $rootScope, $localStorage, $ionicModal, $timeout) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // logout

        $scope.logout = function () {

            API.logout({

                "SessionID": $localStorage.session

            }).then(function (data) {

                $localStorage.session = "";
                $rootScope.unreadInbox = 0;
                $rootScope.tickets = [];
                $rootScope.messages = [];
                $rootScope.inbox = [];
                $rootScope.robotAccountsList = [];
                $rootScope.robotAccountsState = [];
                $rootScope.activeRobots = {};
                $rootScope.accountsList = [];
                $rootScope.information = {};

                $state.go('app.login')

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

    })

    .controller('EnterCtrl', function($translate, API, $ionicPopup, $ionicModal, $ionicSideMenuDelegate, $rootScope, $scope, $timeout, $state){

        $ionicSideMenuDelegate.canDragContent(false);

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });


        // Service Ticket = contact us

        $scope.contactData = {
            "name": "",
            "subject": "",
            "phone": "",
            "email": ""
        };

        $scope.openModalContactUs = function () {

            $ionicModal.fromTemplateUrl('templates/modal_contact_us.html', {

                scope: $scope,
                animation: 'slide-in-up'

            }).then(function (modal) {

                $scope.modal = modal;
                $scope.modal.show();

            });

        };

        $scope.hideModalContactUs = function () {

            $scope.modal.hide();

        };

        $scope.contactUs = function () {

            var emailRegex = /\S+@\S+\.\S+/;

            if ($scope.contactData.name === "" || $scope.contactData.subject === "" || $scope.contactData.email === "") {

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if (!emailRegex.test($scope.contactData.email)) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR12"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                API.contact({

                    "ComplainerName": $scope.contactData.name,
                    "SubjectComplain": $scope.contactData.subject,
                    "phone": $scope.contactData.phone,
                    "Email": $scope.contactData.email

                }).then(function (data) {

                    $scope.modal.hide();

                    $ionicPopup.alert({
                        title: $translate.instant("ERR13"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                    $scope.contactData = {
                        "name": "",
                        "subject": "",
                        "phone": "",
                        "email": ""
                    };

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }
        }

    })

    .controller('RegisterCtrl', function ($timeout, $translate, $ionicModal, $ionicSideMenuDelegate, API, $scope, $http, $localStorage, $state, $rootScope, $ionicPopup, $timeout) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        $ionicSideMenuDelegate.canDragContent(false);

        // APP.REGISTER

        $scope.register = {

            "firstname": '',
            "lastname": '',
            "email": '',
            "password": '',
            "password_confirmation": '',
            "birthday": '',
            "phone": '',
            "country": 'United States of America (the)',
            "accept": ''

        };

        $scope.form = {};

        // Validation

        $scope.patterns = {

            // onlyLetters: '[a-zA-Z]+',

            email: (function () {

                return {

                    test: function (value) {
                        return /\S+@\S+\.\S+/.test(value);
                    }

                }

            })(),

            password: (function () {

                return {

                    test: function (value) {
                        return value.replace(/[^0-9]/g, '').length >= 2 && value.replace(/[^a-zA-Z]/g, '').length >= 3 && value.length >= 6;
                    }

                };

            })(),


            password_confirmation: (function () {

                return {

                    test: function (value) {
                        return value === $scope.register.password;
                    }

                }

            })(),

            // phone: (function () {
            //
            //     return {
            //
            //         test: function (value) {
            //
            //             return value.length >= 7 && value.length <= 13;
            //
            //         }
            //
            //     }
            //
            // })(),

            birthday: (function () {

                return {

                    test: function (value) {

                        return $scope.getAge(value) >= 18;

                    }

                }

            })()

        };

        $scope.getAge = function (dateString) {

            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();

            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {

                age--;

            }

            return age;

        };

        // datepicker for birthday

        $scope.newDate = "";

        $scope.pickDate = function () {

            var options = {
                date: new Date(),
                mode: 'date',
            };

            function onSuccess(data) {

                $scope.newDate = data.getFullYear() + '-' + ("0" + (data.getMonth() + 1)).slice(-2) + '-' + ("0" + data.getDate()).slice(-2);

                $timeout(function () {

                    $scope.register.birthday = $scope.newDate;

                }, 100);

            }

            function onError(error) { // Android only

                $ionicPopup.alert({
                    title: $translate.instant("ERR3"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });
            }

            datePicker.show(options, onSuccess, onError);

        };

        // terms and conditions square

        $scope.square = false;

        $scope.setSquare = function(x) {

            $scope.square = x;

        };

        $scope.checkboxError = false;

        // intl tel input

        $scope.int_tel_input_version = $.fn.intlTelInput.version;
        $scope.angular_version = angular.version.full;
        $scope.version = 'latest, see github';

        // Sending request to Tapper with all user data

        $scope.makeRegistration = function () {

            console.log($scope.register);

            if (!$scope.form.registration.$valid) {

                //  Handle invalid form

                $ionicPopup.alert({
                    title: $translate.instant("ERR4"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                return;

            } else if ($scope.form.registration.$valid && $scope.square === false) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR4"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                $scope.checkboxError = true;

                return;

            } else if (typeof $scope.register.phone == 'undefined') {

                $ionicPopup.alert({
                    title: $translate.instant("ERR5"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                return;

            } else {

                // Terms and Conditions are accepted

                if ($scope.square === true) {

                    $scope.register.accept = "yes";

                }

                // alert(JSON.stringify($scope.register));
                console.log(JSON.stringify($scope.register));

                // handle valid form

                $scope.send_data = {

                    "push_id" : $rootScope.pushId,
                    "email" : $scope.register.email,
                    "firstname" : $scope.register.firstname,
                    "lastname" : $scope.register.lastname,
                    "password" : $scope.register.password,
                    "phone" : $scope.register.phone,
                    "birthday" : $scope.register.birthday,
                    "country" : $scope.register.country,
                    "accept" : $scope.register.accept

                };

                $http.post($rootScope.tapperHost + "/push.php", $scope.send_data, {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })

                    .then(function(data) {       // on success

                        console.log(data);

                        $localStorage.tapperId = data.data.id;
                        $localStorage.clientEmail = $scope.register.email;
                        $localStorage.clientPassword = $scope.register.password;
                        $localStorage.clientPhone = "+" + $scope.register.phone;
                        $rootScope.clientPhone = $localStorage.clientPhone;

                        $localStorage.step = 0;
                        $rootScope.step = $localStorage.step;

                        $state.go('app.phone');

                        $scope.form.registration.$submitted = false;

                        $scope.register = {

                            "firstname": '',
                            "lastname": '',
                            "email": '',
                            "password": '',
                            "password_confirmation": '',
                            "birthday": '',
                            "phone": '',
                            "country": '',
                            "accept": ''

                        };

                    },

                    function(err){              // on error

                        $ionicPopup.alert({
                            title: $translate.instant("ERR6"),
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    });

            }

        };

        // APP.PHONE

        // send verification phone code

        $scope.showVerificationInput = false;

        $scope.sendSmsData = {

            "phone" : $localStorage.clientPhone,
            "email" : $localStorage.clientEmail,
            "user_id" : $localStorage.tapperId

        };

        $scope.sendSms = function(){

            $scope.showVerificationInput = true;

            $http.post($rootScope.tapperHost + "/sendSms.php", $scope.sendSmsData, {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            })

                .then(function(data){       // on success

                        console.log(data);

                        $ionicPopup.alert({
                            title: $translate.instant("ERR7"),
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    },

                    function(err){          // on error

                        $ionicPopup.alert({
                            title: $translate.instant("ERR8"),
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    });

        };

        $scope.sendCodeData = {

            "email" : $localStorage.clientEmail,
            "code" : ""

        };

        $scope.checkCode = function(){

            console.log($scope.sendCodeData);

            $http.post($rootScope.tapperHost + "/checkCode.php", $scope.sendCodeData, {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

            })

                .then(function(data){       // on success

                        console.log(data);

                        if (data.data.response == 'succeed') {

                            $localStorage.phoneVerified = true;
                            $rootScope.phoneVerified = $localStorage.phoneVerified;
                            // $scope.showVerificationInput = false;

                            $localStorage.step = 1;
                            $rootScope.step = $localStorage.step;

                            $state.go('app.chooseATF');

                            $ionicPopup.alert({
                                title: $translate.instant("ERR9"),
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                            $scope.sendCodeData = {

                                "email" : $localStorage.clientEmail,
                                "code" : ""

                            };

                        } else {

                            $ionicPopup.alert({
                                title: $translate.instant("ERR10"),
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                        }

                    },

                    function(err){          // on error

                        $ionicPopup.alert({
                            title: $translate.instant("ERR6"),
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    });

        };


        // modal Contact Us

        $scope.contactData = {
            "name": "",
            "subject": "",
            "phone": "",
            "email": ""
        };

        $scope.openModalContactUs = function () {

            $ionicModal.fromTemplateUrl('templates/modal_contact_us.html', {

                scope: $scope,
                animation: 'slide-in-up'

            }).then(function (modal) {

                $scope.modal = modal;
                $scope.modal.show();

            });

        };

        $scope.hideModalContactUs = function () {

            $scope.modal.hide();

        };

        $scope.contactUs = function () {

            var emailRegex = /\S+@\S+\.\S+/;

            if ($scope.contactData.name === "" || $scope.contactData.subject === "" || $scope.contactData.email === "") {

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if (!emailRegex.test($scope.contactData.email)) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR12"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                API.contact({

                    "ComplainerName": $scope.contactData.name,
                    "SubjectComplain": $scope.contactData.subject,
                    "Phone": $scope.contactData.phone,
                    "Email": $scope.contactData.email

                }).then(function (data) {

                    $scope.modal.hide();

                    $ionicPopup.alert({
                        title: $translate.instant("ERR13"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    }).then(function(){ $state.go("app.login") });

                    $scope.contactData = {
                        "name": "",
                        "subject": "",
                        "phone": "",
                        "email": ""
                    };

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }
        };

        // APP.CHOOSEATF

        $scope.changeRobot = function(x){

            $rootScope.firstRobot = x;

        };

        $scope.options = {
            chart: {
                type: 'lineChart',
                height: 200,
                margin : {
                    top: 10,
                    right: 10,
                    bottom: 40,
                    left: 50
                },
                x: function(d){ return d[0]; },
                y: function(d){ return d[1]; },
                yDomain: [0,100],
                clipEdge: true,
                duration: 300,
                useInteractiveGuideline: false,
                clipVoronoi: false,
                useVoronoi: false,
                showControls: false,
                showLegend: false,
                tooltip: {enabled: false},
                xAxis: {
                    tickFormat: function(d) {
                        return d3.time.format('%d/%m')(new Date(d));
                    },
                    ticks: 5,
                    showMaxMin: false,
                },

                yAxis: {
                    tickFormat: function(d){
                        return d3.format('.2')(d) + '%';
                    },
                    axisLabelDistance: 0
                },
                // zoom: {
                //     enabled: true,
                //     scale: 1,
                //     scaleExtent: [1, 5],
                //     useFixedDomain: false,
                //     useNiceScale: false,
                //     horizontalOff: false,
                //     verticalOff: true,
                //     unzoomEventType: 'dblclick.zoom'
                // }
            }
        };


        // $scope.firstDataAdam = [
        //     {
        //         values: [ [ 1083297600000 , -2.974623048543] , [ 1085976000000 , -1.7740300785979] , [ 1088568000000 , 4.4681318138177] , [ 1091246400000 , 7.0242541001353] , [ 1093924800000 , 7.5709603667586] , [ 1096516800000 , 20.612245065736] , [ 1099195200000 , 21.698065237316] , [ 1101790800000 , 40.501189458018] , [ 1104469200000 , 50.464679413194] , [ 1107147600000 , 48.917421973355] , [ 1109566800000 , 63.750936549160] , [ 1112245200000 , 59.072499126460] , [ 1114833600000 , 43.373158880492] , [ 1117512000000 , 54.490918947556] , [ 1120104000000 , 56.661178852079] , [ 1122782400000 , 73.450103545496] , [ 1125460800000 , 71.714526354907] , [ 1128052800000 , 85.221664349607] , [ 1130734800000 , 77.769261392481] , [ 1133326800000 , 95.966528716500] , [ 1136005200000 , 107.59132116397] , [ 1138683600000 , 127.25740096723] , [ 1141102800000 , 122.13917498830] , [ 1143781200000 , 126.53657279774] , [ 1146369600000 , 132.39300992970] , [ 1149048000000 , 120.11238242904] , [ 1151640000000 , 118.41408917750] , [ 1154318400000 , 107.92918924621] , [ 1156996800000 , 110.28057249569] , [ 1159588800000 , 117.20485334692] , [ 1162270800000 , 141.33556756948] , [ 1164862800000 , 159.59452727893] , [ 1167541200000 , 167.09801853304] , [ 1170219600000 , 185.46849659215] , [ 1172638800000 , 184.82474099990] , [ 1175313600000 , 195.63155213887] , [ 1177905600000 , 207.40597044171] , [ 1180584000000 , 230.55966698196] , [ 1183176000000 , 239.55649035292] , [ 1185854400000 , 241.35915085208] , [ 1188532800000 , 239.89428956243] , [ 1191124800000 , 260.47781917715] , [ 1193803200000 , 276.39457482225] , [ 1196398800000 , 258.66530682672] , [ 1199077200000 , 250.98846121893] , [ 1201755600000 , 226.89902618127] , [ 1204261200000 , 227.29009273807] , [ 1206936000000 , 218.66476654350] , [ 1209528000000 , 232.46605902918] , [ 1212206400000 , 253.25667081117] , [ 1214798400000 , 235.82505363925] , [ 1217476800000 , 229.70112774254] , [ 1220155200000 , 225.18472705952] , [ 1222747200000 , 189.13661746552] , [ 1225425600000 , 149.46533007301] , [ 1228021200000 , 131.00340772114] , [ 1230699600000 , 135.18341728866] , [ 1233378000000 , 109.15296887173] , [ 1235797200000 , 84.614772549760] , [ 1238472000000 , 100.60810015326] , [ 1241064000000 , 141.50134895610] , [ 1243742400000 , 142.50405083675] , [ 1246334400000 , 139.81192372672] , [ 1249012800000 , 177.78205544583] , [ 1251691200000 , 194.73691933074] , [ 1254283200000 , 209.00838460225] , [ 1256961600000 , 198.19855877420] , [ 1259557200000 , 222.37102417812] , [ 1262235600000 , 234.24581081250] , [ 1264914000000 , 228.26087689346] , [ 1267333200000 , 248.81895126250] , [ 1270008000000 , 270.57301075186] , [ 1272600000000 , 292.64604322550] , [ 1275278400000 , 265.94088520518] , [ 1277870400000 , 237.82887467569] , [ 1280548800000 , 265.55973314204] , [ 1283227200000 , 248.30877330928] , [ 1285819200000 , 278.14870066912] , [ 1288497600000 , 292.69260960288] , [ 1291093200000 , 300.84263809599] , [ 1293771600000 , 326.17253914628] , [ 1296450000000 , 337.69335966505] , [ 1298869200000 , 339.73260965121] , [ 1301544000000 , 346.87865120765] , [ 1304136000000 , 347.92991526628] , [ 1306814400000 , 342.04627502669] , [ 1309406400000 , 333.45386231233] , [ 1312084800000 , 323.15034181243] , [ 1314763200000 , 295.66126882331] , [ 1317355200000 , 251.48014579253] , [ 1320033600000 , 295.15424257905] , [ 1322629200000 , 294.54766764397] , [ 1325307600000 , 295.72906119051] , [ 1327986000000 , 325.73351347613] , [ 1330491600000 , 340.16106061186] , [ 1333166400000 , 345.15514071490] , [ 1335758400000 , 337.10259395679] , [ 1338436800000 , 318.68216333837] , [ 1341028800000 , 317.03683945246] , [ 1343707200000 , 318.53549659997] , [ 1346385600000 , 332.85381464104] , [ 1348977600000 , 337.36534373477] , [ 1351656000000 , 350.27872156161] , [ 1354251600000 , 349.45128876100]],
        //         color: "#647D4B"
        //     }
        // ];
        //
        // $scope.firstDataEmily = [
        //     {
        //         values: [ [ 1083297600000 , -0.77078283705125] , [ 1085976000000 , -1.8356366650335] , [ 1088568000000 , -5.3121322073127] , [ 1091246400000 , -4.9320975829662] , [ 1093924800000 , -3.9835408823225] , [ 1096516800000 , -6.8694685316805] , [ 1099195200000 , -8.4854877428545] , [ 1101790800000 , -15.933627197384] , [ 1104469200000 , -15.920980069544] , [ 1107147600000 , -12.478685045651] , [ 1109566800000 , -17.297761889305] , [ 1112245200000 , -15.247129891020] , [ 1114833600000 , -11.336459046839] , [ 1117512000000 , -13.298990907415] , [ 1120104000000 , -16.360027000056] , [ 1122782400000 , -18.527929522030] , [ 1125460800000 , -22.176516738685] , [ 1128052800000 , -23.309665368330] , [ 1130734800000 , -21.629973409748] , [ 1133326800000 , -24.186429093486] , [ 1136005200000 , -29.116707312531] , [ 1138683600000 , -37.188037874864] , [ 1141102800000 , -34.689264821198] , [ 1143781200000 , -39.505932105359] , [ 1146369600000 , -45.339572492759] , [ 1149048000000 , -43.849353192764] , [ 1151640000000 , -45.418353922571] , [ 1154318400000 , -44.579281059919] , [ 1156996800000 , -44.027098363370] , [ 1159588800000 , -41.261306759439] , [ 1162270800000 , -47.446018534027] , [ 1164862800000 , -53.413782948909] , [ 1167541200000 , -50.700723647419] , [ 1170219600000 , -56.374090913296] , [ 1172638800000 , -61.754245220322] , [ 1175313600000 , -66.246241587629] , [ 1177905600000 , -75.351650899999] , [ 1180584000000 , -81.699058262032] , [ 1183176000000 , -82.487023368081] , [ 1185854400000 , -86.230055113277] , [ 1188532800000 , -84.746914818507] , [ 1191124800000 , -100.77134971977] , [ 1193803200000 , -109.95435565947] , [ 1196398800000 , -99.605672965057] , [ 1199077200000 , -99.607249394382] , [ 1201755600000 , -94.874614950188] , [ 1204261200000 , -105.35899063105] , [ 1206936000000 , -106.01931193802] , [ 1209528000000 , -110.28883571771] , [ 1212206400000 , -119.60256203030] , [ 1214798400000 , -115.62201315802] , [ 1217476800000 , -106.63824185202] , [ 1220155200000 , -99.848746318951] , [ 1222747200000 , -85.631219602987] , [ 1225425600000 , -63.547909262067] , [ 1228021200000 , -59.753275364457] , [ 1230699600000 , -63.874977883542] , [ 1233378000000 , -56.865697387488] , [ 1235797200000 , -54.285579501988] , [ 1238472000000 , -56.474659581885] , [ 1241064000000 , -63.847137745644] , [ 1243742400000 , -68.754247867325] , [ 1246334400000 , -69.474257009155] , [ 1249012800000 , -75.084828197067] , [ 1251691200000 , -77.101028237237] , [ 1254283200000 , -80.454866854387] , [ 1256961600000 , -78.984349952220] , [ 1259557200000 , -83.041230807854] , [ 1262235600000 , -84.529748348935] , [ 1264914000000 , -83.837470195508] , [ 1267333200000 , -87.174487671969] , [ 1270008000000 , -90.342293007487] , [ 1272600000000 , -93.550928464991] , [ 1275278400000 , -85.833102140765] , [ 1277870400000 , -79.326501831592] , [ 1280548800000 , -87.986196903537] , [ 1283227200000 , -85.397862121771] , [ 1285819200000 , -94.738167050020] , [ 1288497600000 , -98.661952897151] , [ 1291093200000 , -99.609665952708] , [ 1293771600000 , -103.57099836183] , [ 1296450000000 , -104.04353411322] , [ 1298869200000 , -108.21382792587] , [ 1301544000000 , -108.74006900920] , [ 1304136000000 , -112.07766650960] , [ 1306814400000 , -109.63328199118] , [ 1309406400000 , -106.53578966772] , [ 1312084800000 , -103.16480871469] , [ 1314763200000 , -95.945078001828] , [ 1317355200000 , -81.226687340874] , [ 1320033600000 , -90.782206596168] , [ 1322629200000 , -89.484445370113] , [ 1325307600000 , -88.514723135326] , [ 1327986000000 , -93.381292724320] , [ 1330491600000 , -97.529705609172] , [ 1333166400000 , -99.520481439189] , [ 1335758400000 , -99.430184898669] , [ 1338436800000 , -93.349934521973] , [ 1341028800000 , -95.858475286491] , [ 1343707200000 , -95.522755836605] , [ 1346385600000 , -98.503848862036] , [ 1348977600000 , -101.49415251896] , [ 1351656000000 , -101.50099325672] , [ 1354251600000 , -99.487094927489]],
        //         color: "#C42126"
        //     }
        // ];
        //
        // $scope.firstDataBruno = [
        //     {
        //         values: [ [ 1083297600000 , -3.7454058855943] , [ 1085976000000 , -3.6096667436314] , [ 1088568000000 , -0.8440003934950] , [ 1091246400000 , 2.0921565171691] , [ 1093924800000 , 3.5874194844361] , [ 1096516800000 , 13.742776534056] , [ 1099195200000 , 13.212577494462] , [ 1101790800000 , 24.567562260634] , [ 1104469200000 , 34.543699343650] , [ 1107147600000 , 36.438736927704] , [ 1109566800000 , 46.453174659855] , [ 1112245200000 , 43.825369235440] , [ 1114833600000 , 32.036699833653] , [ 1117512000000 , 41.191928040141] , [ 1120104000000 , 40.301151852023] , [ 1122782400000 , 54.922174023466] , [ 1125460800000 , 49.538009616222] , [ 1128052800000 , 61.911998981277] , [ 1130734800000 , 56.139287982733] , [ 1133326800000 , 71.780099623014] , [ 1136005200000 , 78.474613851439] , [ 1138683600000 , 90.069363092366] , [ 1141102800000 , 87.449910167102] , [ 1143781200000 , 87.030640692381] , [ 1146369600000 , 87.053437436941] , [ 1149048000000 , 76.263029236276] , [ 1151640000000 , 72.995735254929] , [ 1154318400000 , 63.349908186291] , [ 1156996800000 , 66.253474132320] , [ 1159588800000 , 75.943546587481] , [ 1162270800000 , 93.889549035453] , [ 1164862800000 , 106.18074433002] , [ 1167541200000 , 116.39729488562] , [ 1170219600000 , 129.09440567885] , [ 1172638800000 , 123.07049577958] , [ 1175313600000 , 129.38531055124] , [ 1177905600000 , 132.05431954171] , [ 1180584000000 , 148.86060871993] , [ 1183176000000 , 157.06946698484] , [ 1185854400000 , 155.12909573880] , [ 1188532800000 , 155.14737474392] , [ 1191124800000 , 159.70646945738] , [ 1193803200000 , 166.44021916278] , [ 1196398800000 , 159.05963386166] , [ 1199077200000 , 151.38121182455] , [ 1201755600000 , 132.02441123108] , [ 1204261200000 , 121.93110210702] , [ 1206936000000 , 112.64545460548] , [ 1209528000000 , 122.17722331147] , [ 1212206400000 , 133.65410878087] , [ 1214798400000 , 120.20304048123] , [ 1217476800000 , 123.06288589052] , [ 1220155200000 , 125.33598074057] , [ 1222747200000 , 103.50539786253] , [ 1225425600000 , 85.917420810943] , [ 1228021200000 , 71.250132356683] , [ 1230699600000 , 71.308439405118] , [ 1233378000000 , 52.287271484242] , [ 1235797200000 , 30.329193047772] , [ 1238472000000 , 44.133440571375] , [ 1241064000000 , 77.654211210456] , [ 1243742400000 , 73.749802969425] , [ 1246334400000 , 70.337666717565] , [ 1249012800000 , 102.69722724876] , [ 1251691200000 , 117.63589109350] , [ 1254283200000 , 128.55351774786] , [ 1256961600000 , 119.21420882198] , [ 1259557200000 , 139.32979337027] , [ 1262235600000 , 149.71606246357] , [ 1264914000000 , 144.42340669795] , [ 1267333200000 , 161.64446359053] , [ 1270008000000 , 180.23071774437] , [ 1272600000000 , 199.09511476051] , [ 1275278400000 , 180.10778306442] , [ 1277870400000 , 158.50237284410] , [ 1280548800000 , 177.57353623850] , [ 1283227200000 , 162.91091118751] , [ 1285819200000 , 183.41053361910] , [ 1288497600000 , 194.03065670573] , [ 1291093200000 , 201.23297214328] , [ 1293771600000 , 222.60154078445] , [ 1296450000000 , 233.35556801977] , [ 1298869200000 , 231.22452435045] , [ 1301544000000 , 237.84432503045] , [ 1304136000000 , 235.55799131184] , [ 1306814400000 , 232.11873570751] , [ 1309406400000 , 226.62381538123] , [ 1312084800000 , 219.34811113539] , [ 1314763200000 , 198.69242285581] , [ 1317355200000 , 168.90235629066] , [ 1320033600000 , 202.64725756733] , [ 1322629200000 , 203.05389378105] , [ 1325307600000 , 204.85986680865] , [ 1327986000000 , 229.77085616585] , [ 1330491600000 , 239.65202435959] , [ 1333166400000 , 242.33012622734] , [ 1335758400000 , 234.11773262149] , [ 1338436800000 , 221.47846307887] , [ 1341028800000 , 216.98308827912] , [ 1343707200000 , 218.37781386755] , [ 1346385600000 , 229.39368622736] , [ 1348977600000 , 230.54656412916] , [ 1351656000000 , 243.06087025523] , [ 1354251600000 , 244.24733578385]],
        //         color: "#184275"
        //     }
        // ];

        $scope.firstRobotStatistics = [];
        $scope.firstDataAdam = [{ values : [], color: "#647D4B"}];
        $scope.firstDataEmily = [{ values : [], color: "#C42126"}];
        $scope.firstDataBruno = [{ values : [], color: "#184275"}];

        // get first robot statistics

        $http.post($rootScope.tapperHost + "/getRobotStatistics.php", '', {

            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        })

            .then(function (data) {   // on success

                    $rootScope.firstRobotStatistics = data.data;

                    $scope.firstDataAdam[0].values = $rootScope.firstRobotStatistics['adam'];
                    $scope.firstDataEmily[0].values = $rootScope.firstRobotStatistics['emily'];
                    $scope.firstDataBruno[0].values = $rootScope.firstRobotStatistics['bruno'];

                    console.log("First Robot Statistics", $rootScope.firstRobotStatistics);

                },

                function (err) {      // on error

                    console.log(err);

                });

        $scope.chooseFirstRobot = function(){

            // decoding its name to the right format

            if ($rootScope.firstRobot == "adam"){

                $rootScope.firstRobotTPname = $rootScope.adamTPname;

            } else if ($rootScope.firstRobot == "emily"){

                $rootScope.firstRobotTPname = $rootScope.emilyTPname;

            } else if ($rootScope.firstRobot == "bruno"){

                $rootScope.firstRobotTPname = $rootScope.brunoTPname;

            }

            // updating table at Tapper and getting all necessary data for the registration

            var send_step = {

                "email" : $localStorage.clientEmail,
                "step" : 2

            };

            $http.post($rootScope.tapperHost + "/setStep.php", send_step, {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            })

                .then(function(data){       // on success

                        console.log(data.data);

                        if (data.data.response == "succeed"){

                            // if everything is OK - making registration at Hyper

                            API.register({

                                "FirstName": data.data.user.firstname,
                                "LastName": data.data.user.lastname,
                                "Email": data.data.user.email,
                                "Password": data.data.user.password,
                                "FirstPhone": "+" + data.data.user.phone,
                                // "Birthday": ($scope.register.birthday.getDate() < 10 ? '0' : '') + $scope.register.birthday.getDate() + '/' + ($scope.register.birthday.getMonth() < 10 ? '0' : '') + ($scope.register.birthday.getMonth() + 1) + '/' + $scope.register.birthday.getFullYear(),
                                "Birthday": data.data.user.birthday,
                                "Country": data.data.user.country,
                                "ConfirmTermsOfUse": data.data.user.accept,
                                "ConfirmTradeContract": data.data.user.accept,
                                "Robot" : $scope.firstRobotTPname

                            }).then(
                                function (data) {       // on success

                                    $localStorage.step = 2;
                                    $rootScope.step = $localStorage.step;

                                    $localStorage.session = data.Cookie;
                                    console.log($localStorage.clientEmail, $localStorage.clientPassword, $localStorage.session);

                                    $rootScope.getInbox();
                                    $rootScope.getPersonalInformation();
                                    $rootScope.getRobotAccounts();

                                    $state.go('app.deposit');

                                    // updating tapper DB about opening robot

                                    var send_data = {

                                        "id": $localStorage.tapperId,
                                        "robot": $rootScope.firstRobot

                                    };

                                    $http.post($rootScope.tapperHost + "/openRobot.php", send_data, {

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                                    })

                                        .then(function (data) {   // on success

                                                console.log(data);

                                            },

                                            function (err) {      // on error

                                                // alert(err);

                                            })

                                },

                                function (data) {           // on error - not standard message because of not stanard errors

                                    if(data.error.ResponseCode === "006"){

                                        $ionicPopup.alert({
                                            title: $translate.instant("ERR1"),  // session is over
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        }).then(function(){

                                            $state.go('app.login')

                                        });

                                    } else if(data.error.ResponseCode === "058"){

                                        $ionicPopup.alert({
                                            title: $translate.instant("ERR34"), // account already exists
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        }).then(function(){

                                            $state.go('app.login')

                                        });

                                    } else if(data.error.ResponseCode === "050"){

                                        $ionicPopup.alert({
                                            title: $translate.instant("ERR35"), // email is invalid
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        }).then(function(){

                                            $state.go('app.register')

                                        });

                                    } else if (data.error) {

                                        $ionicPopup.alert({
                                            title: data.error.ResponseMsgEng,
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                    } else {

                                        $ionicPopup.alert({
                                            title: $translate.instant("ERR2"),
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                    }

                                });

                        } else {

                            $ionicPopup.alert({
                                title: $translate.instant("ERR6"),
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                        }

                    },

                    function(err){          // on error

                        $ionicPopup.alert({
                            title: $translate.instant("ERR6"),
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    });

        };

    })

    .controller('LoginCtrl', function ($timeout, $translate, $ionicSideMenuDelegate, API, $scope, $localStorage, $ionicPopup, $ionicModal, $http, $rootScope, $state) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        // $ionicSideMenuDelegate.canDragContent(false);

        // login

        $scope.login = {

            "email": $localStorage.clientEmail,
            "password": $localStorage.clientPassword

        };

        $scope.loginAnswer = {};

        $scope.makeLogin = function () {

            // check if email is valid

            var emailRegex = /\S+@\S+\.\S+/;

            if (!emailRegex.test($scope.login.email)) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR12"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if (!$localStorage.step || typeof $localStorage.step == undefined || $localStorage.step != 3 || $localStorage.step != 2){

                // check if user made all steps at registration or simply has a new phone

                var send_email = {

                    "email" : $scope.login.email

                };

                $http.post($rootScope.tapperHost + "/checkStep.php", send_email, {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })

                    .then(function(data){   // on success

                        console.log(data.data);

                            if (data.data.step == 0){   // if user didn't verify his phone

                                $localStorage.step = data.data.step;
                                $rootScope.step = $localStorage.step;
                                $localStorage.clientEmail = $scope.login.email;
                                $localStorage.clientPassword = $scope.login.password;

                                $ionicPopup.alert({
                                    title: $translate.instant("ERR14"),
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                }).then(function(){

                                    $state.go('app.phone');

                                });

                            } else if (data.data.step == 1) {   // if user didn't choose his first ATF

                                $localStorage.step = data.data.step;
                                $rootScope.step = $localStorage.step;
                                $localStorage.clientEmail = $scope.login.email;
                                $localStorage.clientPassword = $scope.login.password;

                                $state.go('app.chooseATF');

                            } else {    // if everything is all right and it's a new phone simply

                                $localStorage.step = data.data.step;
                                $rootScope.step = $localStorage.step;

                                API.login({

                                    "Email": $scope.login.email,
                                    "Password": $scope.login.password

                                }).then(function (data) {

                                    $localStorage.clientEmail = $scope.login.email;
                                    $localStorage.clientPassword = $scope.login.password;
                                    $localStorage.session = data.Cookie;

                                    $rootScope.getInbox();
                                    $rootScope.getPersonalInformation();
                                    $rootScope.getRobotAccounts();

                                    $state.go("app.myaccount");

                                    $scope.login = {

                                        "email": $localStorage.clientEmail,
                                        "password": $localStorage.clientPassword

                                    };

                                    console.log($localStorage.clientEmail, $localStorage.clientPassword, $localStorage.session);

                                    // login for push

                                    var send_data = {

                                        "push_id": $rootScope.pushId,
                                        "email": $localStorage.clientEmail

                                    };

                                    $http.post($rootScope.tapperHost + "/push.php", send_data, {

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                                    })

                                        .then(function (data) {   // on success

                                                // alert(JSON.stringify(data.data));
                                                $localStorage.tapperId = data.data.id;

                                            },

                                            function (err) {      // on error

                                                // alert(err);

                                            })


                                }, function (data) {    // not standard message because not standard error 010

                                    if (data.error.ResponseCode === "010"){

                                        $rootScope.getInbox();
                                        $rootScope.getPersonalInformation();
                                        $rootScope.getRobotAccounts();
                                        $state.go('app.myaccount');

                                    } else if (data.error) {

                                        $ionicPopup.alert({
                                            title: data.error.ResponseMsgEng,
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                    } else {

                                        $ionicPopup.alert({
                                            title: $translate.instant("ERR2"),
                                            buttons: [{
                                                text: 'OK',
                                                type: 'button-positive'
                                            }]
                                        });

                                    }

                                });

                            }

                        },

                        function(err){      // on error

                            $ionicPopup.alert({
                                title: $translate.instant("ERR6"),
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            })

                        });

            } else {    // if everything is all right and it's the same phone

                API.login({

                    "Email": $scope.login.email,
                    "Password": $scope.login.password

                }).then(function (data) {

                    $localStorage.clientEmail = $scope.login.email;
                    $localStorage.clientPassword = $scope.login.password;
                    $localStorage.session = data.Cookie;

                    // get all details

                    $rootScope.getInbox();
                    $rootScope.getPersonalInformation();
                    $rootScope.getRobotAccounts();

                    $state.go("app.myaccount");

                    $scope.login = {

                        "email": $localStorage.clientEmail,
                        "password": $localStorage.clientPassword

                    };

                    console.log($localStorage.clientEmail, $localStorage.clientPassword, $localStorage.session);

                    // login for push

                    var send_data = {

                        "push_id": $rootScope.pushId,
                        "email": $localStorage.clientEmail

                    };

                    $http.post($rootScope.tapperHost + "/push.php", send_data, {

                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })

                        .then(function (data) {   // on success

                                $localStorage.tapperId = data.data.id;

                            },

                            function (err) {      // on error

                                // alert(err);

                            })


                }, function (data) {    // not standard message because not standard error 010

                    if (data.error.ResponseCode === "010"){

                        $state.go('app.myaccount');

                    } else if (data.error) {

                        $ionicPopup.alert({
                            title: data.error.ResponseMsgEng,
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    } else {

                        $ionicPopup.alert({
                            title: $translate.instant("ERR2"),
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    }

                });

            }


        };

        // Forgot password

        $scope.openModalForgotPassword = function () {

            $ionicModal.fromTemplateUrl('templates/modal_forgot_password.html', {

                scope: $scope,
                animation: 'slide-in-up'

            }).then(function (modal) {

                $scope.modal = modal;
                $scope.modal.show();

            });

        };

        $scope.hideModalForgotPassword = function () {

            $scope.modal.hide();

        };

        $scope.forgot = {

            "email": $localStorage.clientEmail

        };

        $scope.sendForgotPassword = function () {

            var emailRegex = /\S+@\S+\.\S+/;

            if (!emailRegex.test($scope.forgot.email)) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR12"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                // request to API

                API.sendForgotPassword({

                    "Email": $scope.forgot.email

                }).then(function (data) {

                    console.log(data);
                    $ionicPopup.alert({
                        title: $translate.instant("ERR15") + $scope.forgot.email,
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });
                    $scope.hideModalForgotPassword();

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }

        };

    })

    .controller('MyAccountCtrl', function ($translate, API, $localStorage, $scope, $state, $rootScope) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

            if ($rootScope.newNotification != ""){

                $rootScope.newNotification = "";

            }

        });

        $scope.intervals = [
            {title : '1M', id : '0'},
            {title : '5M', id : '1'},
            {title : '30M', id : '2'},
            {title : '4H', id : '3'},
            {title : '1D', id : '4'},
            {title : '1W', id : '5'},
            {title : '1M', id : '6'},
            {title : '3M', id : '7'},
            {title : '12M', id : '8'}
        ];

        $scope.selected = {interval : '1'};

        // $scope.$on('newNotificationToAll', function(){
        //
        //     // do something
        //
        // });

    })

    .controller('InvestmentCtrl', function ($http, $interval, $timeout, $ionicLoading, $translate, API, $localStorage, $scope, $state, $rootScope) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

            // if notification is got

            if ($rootScope.newNotification != ""){

                $scope.changeRobot($rootScope.newNotification.additionalData.title.toLowerCase());
                $rootScope.newNotification = "";

            }

            // get all robot accounts

            $rootScope.getRobotAccounts();

            // $timeout(function(){
            //
            //     $scope.api.update();
            //
            // }, 1000);

            // get trades

            $timeout(function(){

                $scope.getOpenTrades();
                // $scope.getClosedTrades();

            }, 2000);



        });

        // $interval(function(){
        //
        //     console.log('timeout');
        //     $rootScope.getRobotAccounts();
        //
        // }, 10000);

        $scope.intervals = [
            {title : '1M', id : '0'},
            {title : '5M', id : '1'},
            {title : '30M', id : '2'},
            {title : '4H', id : '3'},
            {title : '1D', id : '4'},
            {title : '1W', id : '5'},
            {title : '1M', id : '6'},
            {title : '3M', id : '7'},
            {title : '12M', id : '8'}
        ];

        $scope.selected = {interval : '1'};

        // choosing robots

        $rootScope.robot = 'adam';

        $scope.changeRobot = function(x){

            $rootScope.robot = x;

        };

        $scope.options = {
            chart: {
                type: 'lineChart',
                height: 200,
                margin : {
                    top: 10,
                    right: 10,
                    bottom: 30,
                    left: 50
                },
                x: function(d){ return d[0]; },
                y: function(d){ return d[1]; },

                clipEdge: true,
                duration: 300,
                useInteractiveGuideline: false,
                clipVoronoi: false,
                useVoronoi: false,
                showControls: false,
                showLegend: false,
                tooltip: {enabled: false},
                xAxis: {
                    tickFormat: function(d) {
                        return d3.time.format('%d/%m')(new Date(d))
                    },
                    ticks: 5,
                    showMaxMin: false
                },

                yAxis: {
                    tickFormat: function(d){
                        return d3.format('.2')(d);
                    },
                    axisLabelDistance: 0
                }
            }
        };

        // $scope.options = {
        //     chart: {
        //         type: 'lineChart',
        //         height: 200,
        //         margin : {
        //             top: 10,
        //             right: 10,
        //             bottom: 30,
        //             left: 50
        //         },
        //         x: function(d){ return d[0]; },
        //         y: function(d){ return d[1]/100; },
        //
        //         clipEdge: true,
        //         duration: 300,
        //         useInteractiveGuideline: false,
        //         clipVoronoi: false,
        //         useVoronoi: false,
        //         showControls: false,
        //         showLegend: false,
        //         tooltip: {enabled: false},
        //         xAxis: {
        //             tickFormat: function(d) {
        //                 return d3.time.format('%m/%d')(new Date(d))
        //             },
        //             ticks: 3,
        //             showMaxMin: false,
        //         },
        //
        //         yAxis: {
        //             tickFormat: function(d){
        //                 return d3.format(',.1%')(d);
        //             },
        //             axisLabelDistance: 0
        //         },
        //         zoom: {
        //             enabled: true,
        //             scale: 1,
        //             scaleExtent: [1, 10],
        //             useFixedDomain: false,
        //             useNiceScale: false,
        //             horizontalOff: false,
        //             verticalOff: true,
        //             unzoomEventType: 'dblclick.zoom'
        //         }
        //     }
        // };

        $scope.dataAdam = [
            {
                values: [ [ 1083297600000 , -2.974623048543] , [ 1085976000000 , -1.7740300785979] , [ 1088568000000 , 4.4681318138177] , [ 1091246400000 , 7.0242541001353] , [ 1093924800000 , 7.5709603667586] , [ 1096516800000 , 20.612245065736] , [ 1099195200000 , 21.698065237316] , [ 1101790800000 , 40.501189458018] , [ 1104469200000 , 50.464679413194] , [ 1107147600000 , 48.917421973355] , [ 1109566800000 , 63.750936549160] , [ 1112245200000 , 59.072499126460] , [ 1114833600000 , 43.373158880492] , [ 1117512000000 , 54.490918947556] , [ 1120104000000 , 56.661178852079] , [ 1122782400000 , 73.450103545496] , [ 1125460800000 , 71.714526354907] , [ 1128052800000 , 85.221664349607] , [ 1130734800000 , 77.769261392481] , [ 1133326800000 , 95.966528716500] , [ 1136005200000 , 107.59132116397] , [ 1138683600000 , 127.25740096723] , [ 1141102800000 , 122.13917498830] , [ 1143781200000 , 126.53657279774] , [ 1146369600000 , 132.39300992970] , [ 1149048000000 , 120.11238242904] , [ 1151640000000 , 118.41408917750] , [ 1154318400000 , 107.92918924621] , [ 1156996800000 , 110.28057249569] , [ 1159588800000 , 117.20485334692] , [ 1162270800000 , 141.33556756948] , [ 1164862800000 , 159.59452727893] , [ 1167541200000 , 167.09801853304] , [ 1170219600000 , 185.46849659215] , [ 1172638800000 , 184.82474099990] , [ 1175313600000 , 195.63155213887] , [ 1177905600000 , 207.40597044171] , [ 1180584000000 , 230.55966698196] , [ 1183176000000 , 239.55649035292] , [ 1185854400000 , 241.35915085208] , [ 1188532800000 , 239.89428956243] , [ 1191124800000 , 260.47781917715] , [ 1193803200000 , 276.39457482225] , [ 1196398800000 , 258.66530682672] , [ 1199077200000 , 250.98846121893] , [ 1201755600000 , 226.89902618127] , [ 1204261200000 , 227.29009273807] , [ 1206936000000 , 218.66476654350] , [ 1209528000000 , 232.46605902918] , [ 1212206400000 , 253.25667081117] , [ 1214798400000 , 235.82505363925] , [ 1217476800000 , 229.70112774254] , [ 1220155200000 , 225.18472705952] , [ 1222747200000 , 189.13661746552] , [ 1225425600000 , 149.46533007301] , [ 1228021200000 , 131.00340772114] , [ 1230699600000 , 135.18341728866] , [ 1233378000000 , 109.15296887173] , [ 1235797200000 , 84.614772549760] , [ 1238472000000 , 100.60810015326] , [ 1241064000000 , 141.50134895610] , [ 1243742400000 , 142.50405083675] , [ 1246334400000 , 139.81192372672] , [ 1249012800000 , 177.78205544583] , [ 1251691200000 , 194.73691933074] , [ 1254283200000 , 209.00838460225] , [ 1256961600000 , 198.19855877420] , [ 1259557200000 , 222.37102417812] , [ 1262235600000 , 234.24581081250] , [ 1264914000000 , 228.26087689346] , [ 1267333200000 , 248.81895126250] , [ 1270008000000 , 270.57301075186] , [ 1272600000000 , 292.64604322550] , [ 1275278400000 , 265.94088520518] , [ 1277870400000 , 237.82887467569] , [ 1280548800000 , 265.55973314204] , [ 1283227200000 , 248.30877330928] , [ 1285819200000 , 278.14870066912] , [ 1288497600000 , 292.69260960288] , [ 1291093200000 , 300.84263809599] , [ 1293771600000 , 326.17253914628] , [ 1296450000000 , 337.69335966505] , [ 1298869200000 , 339.73260965121] , [ 1301544000000 , 346.87865120765] , [ 1304136000000 , 347.92991526628] , [ 1306814400000 , 342.04627502669] , [ 1309406400000 , 333.45386231233] , [ 1312084800000 , 323.15034181243] , [ 1314763200000 , 295.66126882331] , [ 1317355200000 , 251.48014579253] , [ 1320033600000 , 295.15424257905] , [ 1322629200000 , 294.54766764397] , [ 1325307600000 , 295.72906119051] , [ 1327986000000 , 325.73351347613] , [ 1330491600000 , 340.16106061186] , [ 1333166400000 , 345.15514071490] , [ 1335758400000 , 337.10259395679] , [ 1338436800000 , 318.68216333837] , [ 1341028800000 , 317.03683945246] , [ 1343707200000 , 318.53549659997] , [ 1346385600000 , 332.85381464104] , [ 1348977600000 , 337.36534373477] , [ 1351656000000 , 350.27872156161] , [ 1354251600000 , 349.45128876100]],
                color: "#647D4B"
            }
        ];

        $scope.dataEmily = [
            {
                values: [ [ 1083297600000 , -0.77078283705125] , [ 1085976000000 , -1.8356366650335] , [ 1088568000000 , -5.3121322073127] , [ 1091246400000 , -4.9320975829662] , [ 1093924800000 , -3.9835408823225] , [ 1096516800000 , -6.8694685316805] , [ 1099195200000 , -8.4854877428545] , [ 1101790800000 , -15.933627197384] , [ 1104469200000 , -15.920980069544] , [ 1107147600000 , -12.478685045651] , [ 1109566800000 , -17.297761889305] , [ 1112245200000 , -15.247129891020] , [ 1114833600000 , -11.336459046839] , [ 1117512000000 , -13.298990907415] , [ 1120104000000 , -16.360027000056] , [ 1122782400000 , -18.527929522030] , [ 1125460800000 , -22.176516738685] , [ 1128052800000 , -23.309665368330] , [ 1130734800000 , -21.629973409748] , [ 1133326800000 , -24.186429093486] , [ 1136005200000 , -29.116707312531] , [ 1138683600000 , -37.188037874864] , [ 1141102800000 , -34.689264821198] , [ 1143781200000 , -39.505932105359] , [ 1146369600000 , -45.339572492759] , [ 1149048000000 , -43.849353192764] , [ 1151640000000 , -45.418353922571] , [ 1154318400000 , -44.579281059919] , [ 1156996800000 , -44.027098363370] , [ 1159588800000 , -41.261306759439] , [ 1162270800000 , -47.446018534027] , [ 1164862800000 , -53.413782948909] , [ 1167541200000 , -50.700723647419] , [ 1170219600000 , -56.374090913296] , [ 1172638800000 , -61.754245220322] , [ 1175313600000 , -66.246241587629] , [ 1177905600000 , -75.351650899999] , [ 1180584000000 , -81.699058262032] , [ 1183176000000 , -82.487023368081] , [ 1185854400000 , -86.230055113277] , [ 1188532800000 , -84.746914818507] , [ 1191124800000 , -100.77134971977] , [ 1193803200000 , -109.95435565947] , [ 1196398800000 , -99.605672965057] , [ 1199077200000 , -99.607249394382] , [ 1201755600000 , -94.874614950188] , [ 1204261200000 , -105.35899063105] , [ 1206936000000 , -106.01931193802] , [ 1209528000000 , -110.28883571771] , [ 1212206400000 , -119.60256203030] , [ 1214798400000 , -115.62201315802] , [ 1217476800000 , -106.63824185202] , [ 1220155200000 , -99.848746318951] , [ 1222747200000 , -85.631219602987] , [ 1225425600000 , -63.547909262067] , [ 1228021200000 , -59.753275364457] , [ 1230699600000 , -63.874977883542] , [ 1233378000000 , -56.865697387488] , [ 1235797200000 , -54.285579501988] , [ 1238472000000 , -56.474659581885] , [ 1241064000000 , -63.847137745644] , [ 1243742400000 , -68.754247867325] , [ 1246334400000 , -69.474257009155] , [ 1249012800000 , -75.084828197067] , [ 1251691200000 , -77.101028237237] , [ 1254283200000 , -80.454866854387] , [ 1256961600000 , -78.984349952220] , [ 1259557200000 , -83.041230807854] , [ 1262235600000 , -84.529748348935] , [ 1264914000000 , -83.837470195508] , [ 1267333200000 , -87.174487671969] , [ 1270008000000 , -90.342293007487] , [ 1272600000000 , -93.550928464991] , [ 1275278400000 , -85.833102140765] , [ 1277870400000 , -79.326501831592] , [ 1280548800000 , -87.986196903537] , [ 1283227200000 , -85.397862121771] , [ 1285819200000 , -94.738167050020] , [ 1288497600000 , -98.661952897151] , [ 1291093200000 , -99.609665952708] , [ 1293771600000 , -103.57099836183] , [ 1296450000000 , -104.04353411322] , [ 1298869200000 , -108.21382792587] , [ 1301544000000 , -108.74006900920] , [ 1304136000000 , -112.07766650960] , [ 1306814400000 , -109.63328199118] , [ 1309406400000 , -106.53578966772] , [ 1312084800000 , -103.16480871469] , [ 1314763200000 , -95.945078001828] , [ 1317355200000 , -81.226687340874] , [ 1320033600000 , -90.782206596168] , [ 1322629200000 , -89.484445370113] , [ 1325307600000 , -88.514723135326] , [ 1327986000000 , -93.381292724320] , [ 1330491600000 , -97.529705609172] , [ 1333166400000 , -99.520481439189] , [ 1335758400000 , -99.430184898669] , [ 1338436800000 , -93.349934521973] , [ 1341028800000 , -95.858475286491] , [ 1343707200000 , -95.522755836605] , [ 1346385600000 , -98.503848862036] , [ 1348977600000 , -101.49415251896] , [ 1351656000000 , -101.50099325672] , [ 1354251600000 , -99.487094927489]],
                color: "#C42126"
            }
        ];

        $scope.dataBruno = [
            {
                values: [ [ 1083297600000 , -3.7454058855943] , [ 1085976000000 , -3.6096667436314] , [ 1088568000000 , -0.8440003934950] , [ 1091246400000 , 2.0921565171691] , [ 1093924800000 , 3.5874194844361] , [ 1096516800000 , 13.742776534056] , [ 1099195200000 , 13.212577494462] , [ 1101790800000 , 24.567562260634] , [ 1104469200000 , 34.543699343650] , [ 1107147600000 , 36.438736927704] , [ 1109566800000 , 46.453174659855] , [ 1112245200000 , 43.825369235440] , [ 1114833600000 , 32.036699833653] , [ 1117512000000 , 41.191928040141] , [ 1120104000000 , 40.301151852023] , [ 1122782400000 , 54.922174023466] , [ 1125460800000 , 49.538009616222] , [ 1128052800000 , 61.911998981277] , [ 1130734800000 , 56.139287982733] , [ 1133326800000 , 71.780099623014] , [ 1136005200000 , 78.474613851439] , [ 1138683600000 , 90.069363092366] , [ 1141102800000 , 87.449910167102] , [ 1143781200000 , 87.030640692381] , [ 1146369600000 , 87.053437436941] , [ 1149048000000 , 76.263029236276] , [ 1151640000000 , 72.995735254929] , [ 1154318400000 , 63.349908186291] , [ 1156996800000 , 66.253474132320] , [ 1159588800000 , 75.943546587481] , [ 1162270800000 , 93.889549035453] , [ 1164862800000 , 106.18074433002] , [ 1167541200000 , 116.39729488562] , [ 1170219600000 , 129.09440567885] , [ 1172638800000 , 123.07049577958] , [ 1175313600000 , 129.38531055124] , [ 1177905600000 , 132.05431954171] , [ 1180584000000 , 148.86060871993] , [ 1183176000000 , 157.06946698484] , [ 1185854400000 , 155.12909573880] , [ 1188532800000 , 155.14737474392] , [ 1191124800000 , 159.70646945738] , [ 1193803200000 , 166.44021916278] , [ 1196398800000 , 159.05963386166] , [ 1199077200000 , 151.38121182455] , [ 1201755600000 , 132.02441123108] , [ 1204261200000 , 121.93110210702] , [ 1206936000000 , 112.64545460548] , [ 1209528000000 , 122.17722331147] , [ 1212206400000 , 133.65410878087] , [ 1214798400000 , 120.20304048123] , [ 1217476800000 , 123.06288589052] , [ 1220155200000 , 125.33598074057] , [ 1222747200000 , 103.50539786253] , [ 1225425600000 , 85.917420810943] , [ 1228021200000 , 71.250132356683] , [ 1230699600000 , 71.308439405118] , [ 1233378000000 , 52.287271484242] , [ 1235797200000 , 30.329193047772] , [ 1238472000000 , 44.133440571375] , [ 1241064000000 , 77.654211210456] , [ 1243742400000 , 73.749802969425] , [ 1246334400000 , 70.337666717565] , [ 1249012800000 , 102.69722724876] , [ 1251691200000 , 117.63589109350] , [ 1254283200000 , 128.55351774786] , [ 1256961600000 , 119.21420882198] , [ 1259557200000 , 139.32979337027] , [ 1262235600000 , 149.71606246357] , [ 1264914000000 , 144.42340669795] , [ 1267333200000 , 161.64446359053] , [ 1270008000000 , 180.23071774437] , [ 1272600000000 , 199.09511476051] , [ 1275278400000 , 180.10778306442] , [ 1277870400000 , 158.50237284410] , [ 1280548800000 , 177.57353623850] , [ 1283227200000 , 162.91091118751] , [ 1285819200000 , 183.41053361910] , [ 1288497600000 , 194.03065670573] , [ 1291093200000 , 201.23297214328] , [ 1293771600000 , 222.60154078445] , [ 1296450000000 , 233.35556801977] , [ 1298869200000 , 231.22452435045] , [ 1301544000000 , 237.84432503045] , [ 1304136000000 , 235.55799131184] , [ 1306814400000 , 232.11873570751] , [ 1309406400000 , 226.62381538123] , [ 1312084800000 , 219.34811113539] , [ 1314763200000 , 198.69242285581] , [ 1317355200000 , 168.90235629066] , [ 1320033600000 , 202.64725756733] , [ 1322629200000 , 203.05389378105] , [ 1325307600000 , 204.85986680865] , [ 1327986000000 , 229.77085616585] , [ 1330491600000 , 239.65202435959] , [ 1333166400000 , 242.33012622734] , [ 1335758400000 , 234.11773262149] , [ 1338436800000 , 221.47846307887] , [ 1341028800000 , 216.98308827912] , [ 1343707200000 , 218.37781386755] , [ 1346385600000 , 229.39368622736] , [ 1348977600000 , 230.54656412916] , [ 1351656000000 , 243.06087025523] , [ 1354251600000 , 244.24733578385]],
                color: "#184275"
            }
        ];

        $scope.completeDeposit = function(x, y){

            $rootScope.robotTPname = x;
            $rootScope.$broadcast('completeDeposit', {"clientIndex" : y});
            $state.go('app.deposit');

        };

        $scope.startInvest = function(x, y){

            $ionicLoading.show({
                template: $translate.instant('LAUNCHING')
            });

            API.makerobot({

                "SessionID": $localStorage.session,
                "FirstName": $rootScope.information.FirstName,
                "LastName": $rootScope.information.LastName,
                "Email": $rootScope.information.Email,
                "Password": $rootScope.information.SpareStrFld03,
                "FirstPhone": $rootScope.information['1stPhone'],
                "Birthday": $rootScope.information.Birthday,
                "Country": $rootScope.information.Country,
                "SpareStrFld01" : x

            }).then(function (data) {

                console.log(data);

                $timeout(function(){

                    $rootScope.getRobotAccounts();
                    $ionicLoading.hide();

                }, 5000);

                // updating tapper DB about opening robot

                var send_data = {

                    "id": $localStorage.tapperId,
                    "robot": y

                };

                $http.post($rootScope.tapperHost + "/openRobot.php", send_data, {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })

                    .then(function (data) {   // on success

                            console.log("OpenRobot", data);

                        },

                        function (err) {      // on error

                            // alert(err);

                        })


            }, function (response) {

                $ionicLoading.hide();
                $rootScope.errorMessage(response);

            });

        };

        $scope.openTrades = [];

        $scope.getOpenTrades = function(){

            $scope.openTrades = [];

            API.opentrades({

                "SessionID": $localStorage.session,
                "From_Date": "2016-01-01",
                "Till_Date": "2016-12-12"

            }).then(function (data) {

                $scope.openTrades = data.data.Open_Trades_Report[1].Data;

                if (typeof $rootScope.activeRobots.adam != "undefined"){

                    $scope.dataAdam[0].values = [];

                } else if (typeof $rootScope.activeRobots.emily != "undefined"){

                    $scope.dataEmily[0].values = [];

                } else if (typeof $rootScope.activeRobots.bruno != "undefined"){

                    $scope.dataBruno[0].values = [];

                }

                for (var i = 0; i < $scope.openTrades.length; i++){

                    for (var j = 0; j < $rootScope.accountsList.length; j++){

                        if ($scope.openTrades[i].TradingAccount == $rootScope.accountsList[j].AccountNumber){

                            var value = [Date.parse($scope.openTrades[i].LastUpdate), Number($scope.openTrades[i].TradePL)];

                            if ($rootScope.accountsList[j].SmallRobotName == "adam" && (typeof $rootScope.activeRobots.adam != "undefined")){

                                $scope.dataAdam[0].values.push(value);

                            } else if ($rootScope.accountsList[j].SmallRobotName == "emily" && (typeof $rootScope.activeRobots.emily != "undefined")){

                                $scope.dataEmily[0].values.push(value);

                            } else if ($rootScope.accountsList[j].SmallRobotName == "bruno" && (typeof $rootScope.activeRobots.bruno != "undefined")){

                                $scope.dataBruno.values.push(value);

                            }

                        }

                    }
                }

                console.log("OpenTrades", $scope.openTrades);
                console.log($scope.dataAdam);

            }, function (response){

                $rootScope.errorMessage(response);

            })

        };
        //
        // $scope.getClosedTrades = function(){
        //
        //     API.closedtrades({
        //
        //         "SessionID": $localStorage.session,
        //         "From_Date": "2016-01-01",
        //         "Till_Date": "2016-12-12"
        //
        //     }).then(function (data) {
        //
        //         // console.log("OpenTrades", data.data.Open_Trades_Report[1].Data);
        //     //
        //     }, function (response){
        //
        //         $rootScope.errorMessage(response);
        //
        //     })
        //
        // }

    })

    .controller('ReportsCtrl', function ($translate, $scope, $state, $rootScope) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        $scope.report = {

            "robot" : $rootScope.accountsList[0]

        }

    })

    .controller('DepositCtrl', function ($timeout, $translate, $ionicLoading, $cordovaCamera, API, $scope, $rootScope, $localStorage, $http, $ionicPopup, $state) {

        // Get all accounts of the user

        $scope.$on('$ionicView.enter', function (e) {

            $rootScope.currentState = $state.current.name;

        });

        // Type of payment choosed by the user

        $scope.paymentType = 0;

        $scope.choosePaymentType = function(x){

            $scope.paymentType = x

        };

        // processing credit card form

        $scope.creditCard = {

            "type": '',
            "name": '',
            "number": '',
            "month": '',
            "year": '',
            "cvv": '',
            "amount": "",
            "robot" : ""
        };

        $scope.form = {};

        $scope.types = ['Visa', 'American Express', 'MasterCard'];

        $scope.creditCardDepositResponse = {};

        $scope.makeCreditCardDeposit = function () {

            console.log($rootScope.creditCard);

            if ($rootScope.creditCard.robot == ""){

                $ionicPopup.alert({title: $translate.instant("ERR16")});
                return;

            }

            if ($rootScope.creditCard.amount == ""){

                $ionicPopup.alert({title: $translate.instant("ERR17")});
                return;

            }

            if (!$scope.form.creditCardDepositForm.$valid) {

                console.log('invalid');
                return;

            }

            var exp_date = (($scope.creditCard.month < 10 ? '0' : '') + ($scope.creditCard.month)) + $scope.creditCard.year.toString().substring(2);

            API.creditcarddeposit({

                "SessionID": $localStorage.session,
                "ClientNumber" : $scope.accountsList[0].ClientNumber,
                "TotalAmount" : $scope.creditCard.amount,
                "Currency" : $scope.accountsList[0].UseCurrencyByDefault,
                "CardNumber" : $scope.creditCard.number,
                "Validity" : exp_date,
                "CVV" : $scope.creditCard.cvv,
                "CardHolder" : $scope.creditCard.name

            }).then(function (data) {

                $scope.creditCardDepositResponse = data.data;

                if ($scope.creditCardDepositResponse.TP_Deposit_By_Hyper_json === "Failed"){

                    $ionicPopup.alert({
                        title: $scope.creditCardDepositResponse.Text,
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                } else if ($scope.creditCardDepositResponse.TP_Deposit_By_Hyper_json === "Succeed") {

                    $ionicPopup.alert({
                        title: "You have successfully deposited " + $scope.creditCardDepositResponse["Total Amount"],
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                    $scope.form.creditCardDepositForm.$submitted = false;

                    $scope.creditCard = {

                        "type": '',
                        "name": '',
                        "number": '',
                        "month": '',
                        "year": '',
                        "cvv": '',
                        "amount": ''

                    };

                }

                console.log($scope.creditCardDepositResponse);

                }, function (response) {

                    $rootScope.errorMessage(response);

                });


            // else {
            //
            //     console.log("right data", $rootScope.creditCard);
            //
            //     // TODO: make real deposit
            //
            //     $ionicLoading.show({
            //         template: $translate.instant('MAKINGDEPOSIT')
            //     });
            //
            //     API.fakedeposit({
            //
            //         "SessionID": $localStorage.session,
            //         "ClientNumber": $rootScope.creditCard.robot.ClientNumber,
            //         "TotalAmount": $rootScope.creditCard.amount,
            //         "Currency": "USD",
            //         "Reference" : "12345",
            //         "CC_Brand": "Demo"
            //
            //     }).then(function (data) {
            //
            //         if (data.data.TP_Deposit_CC_by_3rd_Party_json == "Succeed"){        // if everything is OK at Hyper
            //
            //             $timeout(function(){        // wait 5 sec and update data
            //
            //                 $rootScope.getAccountsList();
            //
            //             }, 5000).then(function(){       // update step and inform user
            //
            //                 $localStorage.step = 3;
            //                 $rootScope.step = $localStorage.step;
            //
            //                 $ionicLoading.hide().then(function(){
            //
            //                     $ionicPopup.alert({
            //                         title: $translate.instant("ERR18") + " " + $rootScope.creditCard.amount + " " + $translate.instant("ERR19"),
            //                         buttons: [{
            //                             text: 'OK',
            //                             type: 'button-positive'
            //                         }]
            //                     }).then(function(){
            //
            //                         $state.go('app.investment');
            //
            //                     });
            //
            //                     $rootScope.creditCard.amount = "";
            //
            //                 });
            //
            //             });
            //
            //             // updating tapper DB about making deposit
            //
            //             var send_data = {
            //
            //                 "id": $localStorage.tapperId,
            //                 "robot": $rootScope.creditCard.robot.SmallRobotName,
            //                 "amount" : $rootScope.creditCard.amount
            //
            //             };
            //
            //             $http.post($rootScope.tapperHost + "/makeDeposit.php", send_data, {
            //
            //                 headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            //             })
            //
            //                 .then(function (data) {   // on success
            //
            //                         console.log(data);
            //
            //                     },
            //
            //                     function (err) {      // on error
            //
            //                         // alert(err);
            //
            //                     })
            //
            //         }
            //
            //     }, function (response) {
            //
            //         $ionicLoading.hide().then(function(){
            //
            //             $rootScope.errorMessage(response);
            //
            //         });
            //
            //     });
            //
            // }
        };

        $scope.getJournal = function(){

            API.journal({

                "SessionID": $localStorage.session

            }).then(function (data) {

                console.log(data);

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

        // processing Wire Transfer

        $scope.downloadInfo = function(){

            cordova.InAppBrowser.open("http://www.tapper.co.il/trade/bank_account_info.pdf", '_system', 'location=yes');

        };

        $scope.image = '';

        $scope.wireTransfer = {

            'amount' : '',
            'currency' : ''

        };

        // photo from gallery

        $scope.getPhotoFromGallery = function () {

            var options = {

                quality: 75,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                // targetWidth: 900,
                // targetHeight: 1200,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true

            };

            $cordovaCamera.getPicture(options).then(

                function (data) {

                    $scope.image = data;

                },

                function (err) {

                    $ionicPopup.alert({
                        title: $translate.instant("ERR20"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        // photo from camera

        $scope.getPhotoFromCamera = function () {

            var options = {

                quality: 75,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                // targetWidth: 900,
                // targetHeight: 1200,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true

            };

            $cordovaCamera.getPicture(options).then(
                function (data) {

                    $scope.image = data;

                },

                function (err) {

                    $ionicPopup.alert({
                        title: $translate.instant("ERR20"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });


                })

        };


        $scope.makeWireTransferDeposit = function(){

            if($scope.image == '' || $scope.wireTransfer.currency == '' || $scope.wireTransfer.amount == ''){

                $ionicPopup.alert({
                    title: $translate.instant("ERR21"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                var sendName = $scope.image.substr($scope.image.lastIndexOf("/") + 1);
                // alert(sendName);

                var sendNameExtension = $scope.image.substr($scope.image.lastIndexOf(".") + 1);
                // alert(sendNameExtension);

                var extensions = ["jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "pdf", "PDF", "tif", "TIF", "tiff", "TIFF", "doc", "DOC", "docx", "DOCX"];

                if (extensions.indexOf(sendNameExtension) == -1) {

                    $ionicPopup.alert({
                        title: $translate.instant("ERR22"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                } else {

                    $ionicLoading.show({
                        template: 'Uploading...'
                    });

                    API.wiretransferdeposit($scope.image, {

                        "ApiToken": "LoskiRot160816",
                        "SessionID": $localStorage.session,
                        "ClientNumber": $scope.accountsList[0].ClientNumber,
                        "TotalAmount" : $scope.wireTransfer.amount,
                        "Currency" : $scope.wireTransfer.currency

                    }, sendName)

                        .then(function (data) {

                            $ionicLoading.hide().then(function(){

                                // alert(JSON.stringify(data));

                                if (data.ResponseCode == "022") {

                                    $ionicPopup.alert({
                                        title: data.ResponseMsgEng,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'button-positive'
                                        }]
                                    }).then(function () {

                                        location.reload();
                                        // $state.go('app.inbox');
                                        // $rootScope.getInbox();

                                    });

                                } else {

                                    $ionicPopup.alert({
                                        title: $translate.instant("ERR23"),
                                        buttons: [{
                                            text: 'OK',
                                            type: 'button-positive'
                                        }]
                                    });

                                }

                            });

                        }, function (response) {

                            $ionicLoading.hide().then(function(){

                                // alert(JSON.stringify(response));

                                if (response.ResponseMsgEng) {

                                    $ionicPopup.alert({
                                        title: response.ResponseMsgEng,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'button-positive'
                                        }]
                                    });

                                } else {

                                    $ionicPopup.alert({
                                        title: $translate.instant("ERR23"),
                                        buttons: [{
                                            text: 'OK',
                                            type: 'button-positive'
                                        }]
                                    });

                                }

                            });

                        });

                }

            }

        };

    })

    .controller('WithdrawalCtrl', function ($ionicPopup, $localStorage, API, $translate, $scope, $state, $rootScope) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        $scope.withdrawal = {

            "robot" : $rootScope.accountsList[0],
            "amount": "",
            "method": "1",
            "card": "",
            "scrill": "",
            "neteller": "",
            "bankAccountName": "",
            "bankName": "",
            "bankCountry": "",
            "bankBranch": "",
            "bankSWIFT": "",
            "bankIBAN": ""

        };

        $scope.square = false;

        $scope.setSquare = function(x) {

            $scope.square = x;

        };

        $scope.makeWithdrawal = function(){

            if ($scope.withdrawal.robot == ""){

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if (Number($scope.withdrawal.amount) > Number($scope.withdrawal.robot.SpareExtFld02)){

                $ionicPopup.alert({
                    title: $translate.instant("ERR30"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($scope.withdrawal.method == "1" && $scope.withdrawal.card == ""){

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($scope.withdrawal.method == "2" && ($scope.withdrawal.bankAccountName == "" || $scope.withdrawal.bankName == "" ||
                $scope.withdrawal.bankCountry == "" || $scope.withdrawal.bankBranch == "" || $scope.withdrawal.bankSWIFT == "" ||
                $scope.withdrawal.IBAN == "")){

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($scope.square == false){

                $ionicPopup.alert({
                    title: $translate.instant("TERMSERR"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                console.log($scope.withdrawal);

                API.withdraw({

                    "SessionID": $localStorage.session,
                    "Amount": $scope.withdrawal.amount,
                    "PaymentMethod": $scope.withdrawal.method,
                    "ClientNumber": $scope.withdrawal.robot.ClientNumber

                }).then(function (data) {

                    $ionicPopup.alert({
                        title: $translate.instant("ERR32"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    }).then(function(){$state.go('app.inbox')});

                    $scope.withdrawal = {

                        "robot" : $rootScope.accountsList[0],
                        "amount": "",
                        "method": "1",
                        "card": "",
                        "scrill": "",
                        "neteller": "",
                        "bankAccountName": "",
                        "bankName": "",
                        "bankCountry": "",
                        "bankBranch": "",
                        "bankSWIFT": "",
                        "bankIBAN": ""

                    };

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }



        };

    })

    .controller('HistoryCtrl', function ($localStorage, $ionicPopup, $timeout, API, $translate, $scope, $state, $rootScope) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        $scope.$on('$ionicView.leave', function() {

            $scope.parameters = {

                'type' : '0',
                'from': "",
                'to': ""

            };

            $scope.history = [];

        });


        $scope.parameters = {

            'type' : '0',
            'from': "",
            'to': ""

        };

        // datepicker for start date

        $scope.newStartDate = "";

        $scope.pickStartDate = function () {

            var options = {
                date: new Date(),
                mode: 'date'
            };

            function onSuccess(data) {

                $scope.newStartDate = data.getFullYear() + '-' + ("0" + (data.getMonth() + 1)).slice(-2) + '-' + ("0" + data.getDate()).slice(-2);

                $timeout(function () {

                    $scope.parameters.from = $scope.newStartDate;

                }, 100);

            }

            function onError(error) { // Android only

                $ionicPopup.alert({
                    title: $translate.instant("ERR31"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });
            }

            datePicker.show(options, onSuccess, onError);

        };

        // datepicker for end date

        $scope.newEndDate = "";

        $scope.pickEndDate = function () {

            var options = {
                date: new Date(),
                mode: 'date'
            };

            function onSuccess(data) {

                $scope.newEndDate = data.getFullYear() + '-' + ("0" + (data.getMonth() + 1)).slice(-2) + '-' + ("0" + data.getDate()).slice(-2);

                $timeout(function () {

                    $scope.parameters.to = $scope.newEndDate;

                }, 100);

            }

            function onError(error) { // Android only

                $ionicPopup.alert({
                    title: $translate.instant("ERR31"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });
            }

            datePicker.show(options, onSuccess, onError);

        };

        $scope.history = [];

        $scope.getHistory = function(){

            $scope.history = [];

            if ($scope.parameters.from == "" || $scope.parameters.to == ""){

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                for (var x = 0; x < $rootScope.accountsList.length; x++){

                    API.history({

                        "SessionID": $localStorage.session,
                        "ForAccount": $rootScope.accountsList[x].ClientNumber,
                        "From_Date": $scope.parameters.from,
                        "Till_Date": $scope.parameters.to

                    }).then(function (data) {

                        for (var y = 0; y < data.Data.length; y++){

                            // formatting the entry

                            var record = data.Data[y];
                            record.CreatedOn = Date.parse(record.CreatedOn);
                            record.referenceNumber = record.Comment.substr(record.Comment.indexOf(":") + 1);
                            record.link = "http://portal.infinvesting.com/DownloadSignedDoc?ApiToken=LoskiRot160816&SessionID=" + $localStorage.session + "&Index=" + record.Index + "&ForAccount=" + record.ClientNumber;

                            if (record.Comment.indexOf('Receipt') != -1){

                                record.type = "Deposit";

                            } else if (record.Comment.indexOf('Refund') != -1){

                                record.type = "Withdrawal";

                            } else {

                                record.type = "Document";

                            }

                            switch(record.Status){

                                case '1':

                                    record.Status = "Wasn't sent";
                                    break;

                                case '2':

                                    record.Status = "Waiting";
                                    break;

                                case '3':

                                    record.Status = "Error";
                                    break;

                                case '4':

                                    record.Status = "Completed";
                                    break;

                                case '5':

                                    record.Status = "Waiting";
                                    break;

                                default:

                                    record.Status = "Unknown";
                                    break;

                            }

                            // choosing the right type

                            switch($scope.parameters.type){

                                case '1':

                                    if (record.Comment.indexOf('Receipt') != -1){

                                        $scope.history.push(record);

                                    }
                                    break;

                                case '2':

                                    if (record.Comment.indexOf('Refund') != -1){

                                        $scope.history.push(record);

                                    }
                                    break;

                                case '0':

                                    $scope.history.push(record);
                                    break;

                                default:

                                    $scope.history.push(record);
                                    break;

                            }

                        }

                    }, function (response) {

                        $rootScope.errorMessage(response);

                    });

                }

            }

            console.log("History", $scope.history);

        };

        $scope.getHistoryRecord = function(x){

            console.log(x);
            cordova.InAppBrowser.open(x, '_system', 'location=no');

        }

    })

    .controller('DocumentsCtrl', function ($location, $translate, $ionicLoading, API, $scope, $rootScope, $http, $localStorage, $ionicPopup, $cordovaCamera, $timeout, $window, $state) {

        $scope.documents = {};

        $scope.downloadContract = function () {

            cordova.InAppBrowser.open("http://www.tapper.co.il/trade/atech_contract.pdf", '_system', 'location=yes');

        };

        $scope.$on('$ionicView.beforeEnter', function (e) {

            $rootScope.currentState = $state.current.name;

            // get documents from server

            API.getDocumentsTree({

                "SessionID": $localStorage.session

            }).then(function (data) {

                $scope.documents = data;

                console.log("ctrl", $scope.documents);

                // $scope.folderNames = [];
                // $scope.files = [];
                //
                // for (var i = 0; i < $scope.documents.length; i++) {
                //
                //      $scope.folderNames.push($scope.documents[i].FolderName)
                //
                //     for (var j = 0; j < $scope.documents[i].FilesList.length; j++){
                //
                //         $scope.files.push($scope.documents[i].FilesList[j]);
                //
                //     }
                //
                // }
                // console.log($scope.files);

                // for (var i = 0; i < $scope.documents.length; i++) {

                    // for (var j = 0; j < $scope.documents[i].FilesList.length; j++) {

                        // if ($scope.documents[i].FilesList[j].FileName.substr($scope.documents[i].FilesList[j].FileName.lastIndexOf(".") + 1) === "pdf") {
                        //
                        //     $scope.documents[i].FilesList[j].src = "img/pdf-icon.png";
                        //
                        // } else if ($scope.documents[i].FilesList[j].FileName.substr($scope.documents[i].FilesList[j].FileName.lastIndexOf(".") + 1) === "tiff" || $scope.documents[i].FilesList[j].FileName.substr($scope.documents[i].FilesList[j].FileName.lastIndexOf(".") + 1) === "tif") {
                        //
                        //     $scope.documents[i].FilesList[j].src = "img/tiff-icon.png";
                        //
                        // } else if ($scope.documents[i].FilesList[j].FileName.substr($scope.documents[i].FilesList[j].FileName.lastIndexOf(".") + 1) === "docx" || $scope.documents[i].FilesList[j].FileName.substr($scope.documents[i].FilesList[j].FileName.lastIndexOf(".") + 1) === "doc") {
                        //
                        //     $scope.documents[i].FilesList[j].src = "img/doc-icon.png";
                        //
                        // } else {
                        //
                        //     // $scope.documents[i].FilesList[j].src = "http://portal.infinvesting.com/Download_Doc_json?ApiToken=LoskiRot160816&SessionID=" + $localStorage.session + "&fn=" + $scope.documents[i].FilesList[j].FileName;
                        //
                        //     API.downloadDocument(
                        //         $scope.documents[i].FilesList[j],
                        //         {"SessionID": $localStorage.session, fn: $scope.documents[i].FilesList[j].FileName}
                        //     );
                        //
                        // }

                        // var filename = $scope.documents[i].FilesList[j].FileName.replace(' ', '%20');
                        // $scope.documents[i].FilesList[j].src = "http://portal.infinvesting.com/Download_Doc?SessionID=" + $localStorage.session + "&fn=" + filename;
                        // $scope.documents[i].FilesList[j].src = "http://portal.infinvesting.com/Download_Doc?SessionID=2A00781305219181A60E406D&fn=500003_000004_Doe%20Jane.jpg"

                        // console.log($scope.documents[i].FilesList[j].src);

                        // API.downloadDocument(
                        //     $scope.documents[i].FilesList[j],
                        //     {"SessionID": $localStorage.session, fn: $scope.documents[i].FilesList[j].FileName}
                        // );

                    // }

                // }


            }, function (response) {

                $rootScope.errorMessage(response);

            });

        });

        // photo from gallery

        $scope.getPhotoFromGallery = function (folderName) {

            var options = {

                quality: 75,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                // targetWidth: 900,
                // targetHeight: 1200,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true

            };

            $cordovaCamera.getPicture(options).then(
                function (data) {

                    $scope.uploadFile(data, folderName);

                    $ionicLoading.show({
                        template: 'Uploading...'
                    })

                },

                function (err) {

                    $ionicPopup.alert({
                        title: $translate.instant("ERR20"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        // photo from camera

        $scope.getPhotoFromCamera = function (folderName) {

            var options = {

                quality: 75,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                // targetWidth: 900,
                // targetHeight: 1200,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true

            };

            $cordovaCamera.getPicture(options).then(
                function (data) {

                    $scope.uploadFile(data, folderName);

                    $ionicLoading.show({
                        template: 'Uploading...'
                    })

                },

                function (err) {

                    $ionicPopup.alert({
                        title: $translate.instant("ERR20"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });


                })

        };

        // upload foto

        $scope.uploadFile = function (imageName, folderName) {

            var sendName = imageName.substr(imageName.lastIndexOf("%2F") + 3);
            // alert(sendName);

            var sendNameExtension = sendName.substr(sendName.lastIndexOf(".") + 1);
            // alert(sendNameExtension);

            var extensions = ["jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "pdf", "PDF", "tif", "TIF", "tiff", "TIFF", "doc", "DOC", "docx", "DOCX"];

            if (extensions.indexOf(sendNameExtension) == -1) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR22"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                API.uploadDoc(imageName, {

                    "Category": folderName,
                    "SessionID": $localStorage.session,
                    "ApiToken": "LoskiRot160816"

                }, sendName)

                    .then(function (data) {

                        if (data.Upload_Doc) {

                            $ionicLoading.hide().then(function() {

                                $ionicPopup.alert({
                                    title: $translate.instant("ERR24"),
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                }).then(function () {

                                    location.reload();

                                });

                            })

                        }

                    }, function (response) {

                        if (response.ResponseMsgEng) {

                            $ionicLoading.hide().then(function(){

                                $ionicPopup.alert({
                                    title: response.ResponseMsgEng,
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                });

                            })

                        } else {

                            $ionicLoading.hide().then(function() {

                                $ionicPopup.alert({
                                    title: $translate.instant("ERR28"),
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                });

                            })

                        }

                    });

            }

        };

        // delete photo - only for the same session

        $scope.deleteFile = function (imageName) {

            API.deleteDoc({

                "SessionID": $localStorage.session,
                "fn": imageName

            }).then(function (data) {

                $ionicPopup.alert({
                    title: $translate.instant("ERR25"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                }).then(function(){

                    location.reload();

                });

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

        $scope.showDoc = function(x){

            var filename = x.replace(' ', '%20');

            $scope.image = "http://portal.infinvesting.com/Download_Doc?SessionID=" + $localStorage.session + "&fn=" + filename;

            console.log($scope.image );

            var imagePopup = $ionicPopup.show({
                templateUrl: 'templates/popup_image.html',
                scope: $scope,
                cssClass: 'popupImage'
            });

            $scope.hidePopupImage = function(){

                imagePopup.close();

            }

        }

    })

    .controller('InformationCtrl', function ($translate, $http, API, $scope, $rootScope, $localStorage, $ionicPopup, $state) {

        // get all personal data about the customer

        $scope.$on('$ionicView.enter', function () {

            $rootScope.currentState = $state.current.name;

            $rootScope.getPersonalInformation();

        });

        // change phone or country, if he wants

        // $scope.changeData = {
        //
        //     "country" : ""
        //
        // };
        //
        // $scope.sendChangeData = function(){
        //
        //     API.changedata({
        //
        //         "SessionID": $localStorage.session,
        //         "country" : $scope.changeData.country
        //
        //     }).then(function (data) {
        //
        //         console.log(data);
        //
        //         $ionicPopup.alert({
        //             title: "Your profile information was successfully updated!",
        //             buttons: [{
        //                 text: 'OK',
        //                 type: 'button-positive'
        //             }]
        //         }).then(function(){
        //
        //             location.reload();
        //
        //         });
        //
        //     }, function (response) {
        //
        //         $rootScope.errorMessage(response);
        //
        //     });
        //
        // };

        // change password

        $scope.form = {};

        $scope.changePassword = {

            "currentPassword" : '',
            "newPassword" : '',
            "ConfirmPassword" : ''

        };

        $scope.patterns = {

            password: (function () {

                return {

                    test: function (value) {
                        return value.replace(/[^0-9]/g, '').length >= 2 && value.replace(/[^a-zA-Z]/g, '').length >= 3 && value.length >= 6;
                    }

                };

            })(),


            password_confirmation: (function () {

                return {

                    test: function (value) {
                        return value === $scope.changePassword.newPassword;
                    }

                }

            })()

        };

        $scope.sendChangePassword = function(){

            if (!$scope.form.makeNewPassword.$valid) {

                console.log('Form is invalid');

                return;

            } else {

                console.log('Form is valid');

                API.changepassword({

                    "SessionID": $localStorage.session,
                    "CurrentPassword": $scope.changePassword.currentPassword,
                    "NewPassword": $scope.changePassword.newPassword,
                    "ConfirmPassword": $scope.changePassword.ConfirmPassword

                }).then(function (data) {

                    console.log(data);

                    $localStorage.clientPassword = $scope.changePassword.newPassword;

                    $ionicPopup.alert({
                        title: $translate.instant("ERR26"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                    $scope.form.makeNewPassword.$submitted = false;

                    $scope.changePassword = {

                        "currentPassword" : '',
                        "newPassword" : '',
                        "ConfirmPassword" : ''

                    };

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }

        };

    })

    .controller('SupportCtrl', function ($translate, API, $scope, $rootScope, $localStorage, $ionicPopup, $state) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        $scope.newTicket = {

            "title": "",
            "subject": ""

        };

        $scope.sendNewTicket = function () {

            if ($scope.newTicket.title == "" || $scope.newTicket.subject == ""){

                $ionicPopup.alert({
                    title: $translate.instant("ERR11"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                })

            } else if ($scope.newTicket.title.length > 40){

                $ionicPopup.alert({
                    title: $translate.instant("ERR27"),
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                })

            } else {

                API.newticket({

                    "SessionID": $localStorage.session,
                    "Title": $scope.newTicket.title,
                    "SubjectComplain": $scope.newTicket.subject

                }).then(function (data) {

                    console.log(data);

                    // $rootScope.getInbox();

                    $ionicPopup.alert({
                        title: $translate.instant("ERR13"),
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    }).then(function (res) {

                        $scope.newTicket = {
                            "title": "",
                            "subject": ""
                        };

                        $state.go('app.inbox');

                    });

                }, function (response) {

                    $rootScope.errorMessage(response);

                });

            }

        }

    })

    .controller('InboxCtrl', function ($translate, API, $scope, $localStorage, $rootScope, $state) {

        $scope.$on('$ionicView.enter', function () {

            $rootScope.currentState = $state.current.name;

            $rootScope.getInbox();

        });

        // delete message

        $scope.deleteResponse = "";

        $scope.deleteMessage = function(x) {

            API.deletemessage({

                "SessionID": $localStorage.session,
                "Key" : x

            }).then(function (data) {

                console.log(data);

                $scope.deleteResponse = data.data;

                for (var i = 0; i < $rootScope.messages.length; i++){

                    if ($rootScope.messages[i].Key == $scope.deleteResponse.Key){

                        if ($rootScope.messages[i].Key == $scope.deleteResponse.Key && $rootScope.messages[i].UserReadMessageOn == ""){

                            $rootScope.unreadInbox -= 1;

                        }

                        $rootScope.messages.splice(i, 1);

                    }

                }

                for (var j = 0; j < $rootScope.inbox.length; j++){

                    if ($rootScope.inbox[j].Key == $scope.deleteResponse.Key){

                        $rootScope.inbox.splice(j, 1);

                    }

                }

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

        // make message read

        $scope.readResponse = "";

        $scope.readMessage = function(x) {

            API.readmessage({

                "SessionID": $localStorage.session,
                "Key" : x

            }).then(function (data) {

                $scope.readResponse = data.data;

                console.log($scope.readResponse);

                for (var k = 0; k < $rootScope.messages.length; k++){

                    if ($rootScope.messages[k].Key == $scope.readResponse.Key){

                        $rootScope.messages[k].UserReadMessageOn = "yes";

                        $rootScope.unreadInbox -= 1;

                    }

                }

                for (var u = 0; u < $rootScope.inbox.length; u++){

                    if ($rootScope.inbox[u].Key == $scope.readResponse.Key){

                        $rootScope.inbox[u].UserReadMessageOn = "yes";

                    }

                }

            }, function (response) {

                $rootScope.errorMessage(response);

            });

        };

        // toggling messages

        $scope.shownGroup = null;

        $scope.toggleGroup = function (group) {

            if ($scope.isGroupShown(group)) {

                $scope.shownGroup = null;

            } else {

                $scope.shownGroup = group;

            }
        };

        $scope.isGroupShown = function (group) {

            return $scope.shownGroup === group;

        };

    })

    .controller('NotificationsCtrl', function ($translate, $localStorage, $http, $scope, $rootScope, $state) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        // process new notification

        // $scope.$on('newNotification', function(){

            // $scope.newItem = {};
            // $scope.newItem.text = $rootScope.newNotification.message;
            // $scope.newItem.robot = $rootScope.newNotification.title;
            //
            // $scope.notifications.push($scope.newItem);
            //
            // $rootScope.newNotification = '';

            // location.reload();
        // });

        // get notifications

        $scope.notifications = [];

        $scope.send_data = {

            'id' : $localStorage.tapperId

        };

        $http.post($rootScope.tapperHost + "/getNotifications.php", $scope.send_data, {

            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        })

            .then(function(data){       // on success

                    $scope.notifications = data.data;

                    for (var i = 0; i < $scope.notifications.length; i++){

                        $scope.notifications[i].date = Date.parse($scope.notifications[i].date);

                        if ($scope.notifications[i].robot == "Adam") {

                            $scope.notifications[i].img = "img/adam.png";

                        } else if ($scope.notifications[i].robot == "Emily"){

                            $scope.notifications[i].img = "img/emily.png";

                        } else if ($scope.notifications[i].robot == "Bruno") {

                            $scope.notifications[i].img = "img/bruno.png";

                        } else {

                            $scope.notifications[i].img = "img/notification_icon.png";

                        }

                    }

                    console.log('Notifications', $scope.notifications);

                },

                function(err){          // on error

                    alert(err);

                });

        // animation

        $scope.shownGroup = null;

        $scope.toggleGroup = function (group) {

            if ($scope.isGroupShown(group)) {

                $scope.shownGroup = null;

            } else {

                $scope.shownGroup = group;

            }
        };

        $scope.isGroupShown = function (group) {

            return $scope.shownGroup === group;

        };

    })

    .controller('FAQCtrl', function ($translate, $localStorage, $scope, $rootScope, $http, $state) {

        // get all questions and answer

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

        $scope.FAQ = {};

        $http.get($rootScope.tapperHost + "/getFAQ.php")

            .then(
                function (data) {

                    console.log(data);
                    $scope.FAQ = data.data;

                },
                function (err) {

                    console.log(err);

                });

        // animation

        $scope.shownGroup = null;

        $scope.toggleGroup = function (group) {

            if ($scope.isGroupShown(group)) {

                $scope.shownGroup = null;

            } else {

                $scope.shownGroup = group;

            }
        };

        $scope.isGroupShown = function (group) {

            return $scope.shownGroup === group;

        };
    })

    .controller('TermsCtrl', function ($translate, $ionicHistory, $timeout, $ionicNavBarDelegate, $localStorage, $scope, $rootScope, $http, $state) {

        $scope.$on('$ionicView.enter', function() {

            $rootScope.currentState = $state.current.name;

        });

    });
